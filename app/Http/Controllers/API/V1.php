<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Provinces;
use App\Models\Cities;
use App\Models\Districts;
use App\Models\People;

use Sentinel;

class V1 extends Controller
{
	// Mendapatkan List Kab / Ko [ Parameter : province ]
	public function getCity(Request $request){
		$response['status'] = false;
		$response['message'] = "";
		$response['data'] = [];

		$province = $request->province;
		$city = Cities::where('province_id', $province)->get();
		if(count($city) > 0){
			$response['status'] = true;
			$response['data'] = $city;
			return response()->json($response);
		} else {
			$response['message'] = "Data not Found!";
			return response()->json($response);
		}

	}

	public function getDistrict(Request $request){
		$response['status'] = false;
		$response['message'] = "";
		$response['data'] = [];

		$city = $request->city;

		$district = Districts::where('regency_id', $city)->get();
		if(count($district) > 0 ){
			$response['status'] = true;
			$response['data'] = $district;
			return response()->json($response);
		} else {
			$response['message'] = "Data not Found";
			return response()->json($response);
		}
	}

}
