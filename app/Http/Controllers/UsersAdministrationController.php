<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Sentinel;
use App\Models\Provinces;
use App\Models\Cities;
use App\Models\Districts;
use Carbon\Carbon;

class UsersAdministrationController extends Controller
{


    //------------
    //valid till prevent admin-kecamatan from accessing
    public function index()
    {
      // $userList = Sentinel::getUserRepository()->with('roles')->get();
      $user = Sentinel::getUser();
      $userList = DB::table('users')
                ->select(
                  'users.*',
                  'users.created_at as userscreated_at',
                  'users.updated_at as usersupdated_at',
                  'roles.*',
                  'roles.created_at as rolescreated_at',
                  'roles.updated_at as rolesupdated_at',
                  'role_users.*',
                  'role_users.created_at as role_userscreated_at',
                  'role_users.updated_at as role_usersupdated_at'
                )
                ->join('role_users', 'users.id', '=', 'role_users.user_id')
                ->join('roles', 'role_users.role_id', '=', 'roles.id')
                ->where(function($query){
										if(!empty(Sentinel::getUser()->inRole('super-admin'))){
											$query->where('roles.slug','=','admin-pusat');
										}if(!empty(Sentinel::getUser()->inRole('admin-pusat'))){
											$query->where('roles.slug','=','admin-provinsi');
										}if(!empty(Sentinel::getUser()->inRole('admin-provinsi'))){
                      $query->where('roles.slug','=','admin-kabko');
											$query->where('users.province_id','=',Sentinel::getUser()->province_id);
										}if(!empty(Sentinel::getUser()->inRole('admin-kabko'))){
                      $query->where('roles.slug','=','admin-kecamatan');
											$query->where('users.kab_id','=',Sentinel::getUser()->kab_id);
										}
										return $query;
								})
                ->orderBy('users.id', 'desc')
                ->paginate(10);


      // return for admin kecamatan roles try to access
      if(!empty(Sentinel::getUser()->inRole('admin-kecamatan'))){
          return redirect('/')->with('error',"Halaman yang anda cari tidak ditemukan.");
      }else{
        // return $userList;
        return view('layout.contents.user.index', array(
          'userprofile'=>$user,
          'user'=>$userList
        ));
      }
    }


    //------------
    //this function has disabled from route declaration(web.php), due to functional requirement.
    //this function is not preventing admin-kecamatan from accessing yet
    public function search(Request $request)
    {
      if($request->search == "")
      {
        // $userList = Sentinel::getUserRepository()->with('roles')->get();
        $user = Sentinel::getUser();
        $userList = DB::table('users')
                  ->select(
                    'users.*',
                    'users.created_at as userscreated_at',
                    'users.updated_at as usersupdated_at',
                    'roles.*',
                    'roles.created_at as rolescreated_at',
                    'roles.updated_at as rolesupdated_at',
                    'role_users.*',
                    'role_users.created_at as role_userscreated_at',
                    'role_users.updated_at as role_usersupdated_at'
                  )
                  ->join('role_users', 'users.id', '=', 'role_users.user_id')
                  ->join('roles', 'role_users.role_id', '=', 'roles.id')
                  ->orderBy('users.id', 'desc')
                  ->paginate(10);

        // return $userList;
        return view('layout.contents.user.index', array(
          'userprofile'=>$user,
          'user'=>$userList
        ));
      }else {
        // $userList = Sentinel::getUserRepository()->with('roles')->get();
        $user = Sentinel::getUser();
        $userList = DB::table('users')
                  ->select(
                    'users.*',
                    'users.created_at as userscreated_at',
                    'users.updated_at as usersupdated_at',
                    'roles.*',
                    'roles.created_at as rolescreated_at',
                    'roles.updated_at as rolesupdated_at',
                    'role_users.*',
                    'role_users.created_at as role_userscreated_at',
                    'role_users.updated_at as role_usersupdated_at'
                  )
                  ->join('role_users', 'users.id', '=', 'role_users.user_id')
                  ->join('roles', 'role_users.role_id', '=', 'roles.id')
                  ->where('users.username','LIKE','%'.trim($request->search).'%')
                  ->orWhere('users.email','LIKE','%'.trim($request->search).'%')
                  ->orWhere('roles.name','LIKE','%'.trim($request->search).'%')
                  ->orderBy('users.id', 'desc')
                  ->paginate(10);
        $userList->appends($request->only('search'));
        // return $userList;
        return view('layout.contents.user.index', array(
          'userprofile'=>$user,
          'user'=>$userList
        ));
      }
    }


    //------------
    //valid till prevent admin-kecamatan from accessing
    public function store(Request $request)
    {
      // return for admin kecamatan roles try to access
      if(!empty(Sentinel::getUser()->inRole('admin-kecamatan'))){
          return redirect('/')->with('error',"Halaman yang anda cari tidak ditemukan.");
      }else
      {
        $role = Sentinel::findRoleById($request->role);
        $provinsi = $request->provinsi;
        if (Sentinel::getUser()->inRole('admin-provinsi')) {
          $provinsi = Sentinel::getUser()->province_id;
        }
        $cities = $request->kabko;
        if (Sentinel::getUser()->inRole('admin-kabko')) {
          $provinsi = Sentinel::getUser()->province_id;
          $cities = Sentinel::getUser()->kab_id;
        }
        $credentials = [
          'first_name'    => $request->first_name,
          'last_name'    => $request->last_name,
          'email'    => $request->email,
          'username'    => $request->username,
          'phone_number'    => $request->phone_number,
          'mobile_number'    => $request->mobile_number,
    	    'password' => $request->password,
          'province_id' => $provinsi,
          'kab_id' => $cities,
          'kec_id' => $request->kecamatan,
      	];
      	$user = Sentinel::registerAndActivate($credentials);
      	$role->users()->attach($user);
        $data['error']="false";
        $data['message']="Pengguna baru berhasil disimpan dan diregistrasi.";
        $data['url']=url("usersadministration");
        return response()->json($data);
      }
    }


    //------------
    //valid till prevent admin-kecamatan from accessing
    public function create()
    {
      $roles = \DB::table('roles')
                ->get(array(
                            'roles.id',
                            'roles.slug',
                            'roles.name',
                            'roles.permissions',
                ));
      $provinces = Provinces::all();
      if (Sentinel::getUser()->inRole('admin-provinsi')) {
        $cities = Cities::where('province_id','=',Sentinel::getUser()->province_id)->get();
      }else{
        $cities = Cities::where('province_id','=','-1')->get();
      }
      if (Sentinel::getUser()->inRole('admin-kabko')) {
        $districts = Districts::where('regency_id','=',Sentinel::getUser()->kab_id)->get();
      }else{
        $districts = Districts::where('regency_id','=','-1')->get();
      }
      $userprofile = Sentinel::getUser();

      // return for admin kecamatan roles try to access
      if(!empty(Sentinel::getUser()->inRole('admin-kecamatan'))){
        return redirect('/')->with('error',"Halaman yang anda cari tidak ditemukan.");
      }else{
        return view('layout.contents.user.create', array(
          'userprofile'=>$userprofile,
          'roles'=>$roles,
          'provinces'=>$provinces,
          'cities'=>$cities,
          'districts'=>$districts
        ));
      }
    }


    //------------
    //valid till prevent admin-kecamatan from accessing
    public function show($id)
    {
      $userprofile = Sentinel::getUser();
      $currentchosenuser = DB::table('users')
                ->join('role_users', 'users.id', '=', 'role_users.user_id')
                ->join('roles', 'role_users.role_id', '=', 'roles.id')
                ->where('users.id', '=', $id)
                ->get(array(
                  'users.*',
                  'users.created_at as userscreated_at',
                  'users.updated_at as usersupdated_at',
                  'roles.*',
                  'roles.created_at as rolescreated_at',
                  'roles.updated_at as rolesupdated_at',
                  'role_users.*',
                  'role_users.created_at as role_userscreated_at',
                  'role_users.updated_at as role_usersupdated_at'
                ));
      $roles = \DB::table('roles')
                ->get(array(
                            'roles.id',
                            'roles.slug',
                            'roles.name',
                            'roles.permissions',
                ));
      $return = view('layout.contents.user.show', array(
        'userprofile'=>$userprofile,
        'currentchosenuser'=>$currentchosenuser,
        'roles'=>$roles
      ));

      //bila user mencari id user yang tidak terdaftar
      if(count($currentchosenuser)==0){
          return redirect('usersadministration')->with('error',"Halaman yang anda cari tidak ditemukan.");
      }else{

        // return for super admin roles
        if(!empty(Sentinel::getUser()->inRole('super-admin'))){
          if($currentchosenuser['0']->slug!='admin-pusat'){
            return redirect('usersadministration')->with('error',"Halaman yang anda cari tidak ditemukan.");
          } else{
            return $return;
          }
        }

        // return for admin pusat roles
        if(!empty(Sentinel::getUser()->inRole('admin-pusat'))){
          if($currentchosenuser['0']->slug!='admin-provinsi'){
            return redirect('usersadministration')->with('error',"Halaman yang anda cari tidak ditemukan.");
          } else{
            return $return;
          }
        }

        // return for admin provinsi roles
        if(!empty(Sentinel::getUser()->inRole('admin-provinsi'))){
          if(($currentchosenuser['0']->slug=='admin-kabko')&&($currentchosenuser['0']->province_id==Sentinel::getUser()->province_id)){
            return $return;
          } else{
            return redirect('usersadministration')->with('error',"Halaman yang anda cari tidak ditemukan.");
          }
        }

        // return for admin kabko roles
        if(!empty(Sentinel::getUser()->inRole('admin-kabko'))){
          if(($currentchosenuser['0']->slug=='admin-kecamatan')&&($currentchosenuser['0']->kab_id==Sentinel::getUser()->kab_id)){
            return $return;
          } else{
            return redirect('usersadministration')->with('error',"Halaman yang anda cari tidak ditemukan.");
          }
        }

        // return for admin kecamatan roles try to access
        if(!empty(Sentinel::getUser()->inRole('admin-kecamatan'))){
            return redirect('/')->with('error',"Halaman yang anda cari tidak ditemukan.");
        }

      }
    }


    //------------
    //valid till prevent admin-kecamatan from accessing
    public function showpass($id)
    {
      $userprofile = Sentinel::getUser();
      $currentchosenuser = DB::table('users')
                ->join('role_users', 'users.id', '=', 'role_users.user_id')
                ->join('roles', 'role_users.role_id', '=', 'roles.id')
                ->where('users.id', '=', $id)
                ->get();
      $roles = \DB::table('roles')
                ->get(array(
                            'roles.id',
                            'roles.slug',
                            'roles.name',
                            'roles.permissions',
                ));
      // return $currentchosenuser;
      $return =  view('layout.contents.user.password', array(
        'userprofile'=>$userprofile,
        'currentchosenuser'=>$currentchosenuser,
        'roles'=>$roles
      ));

      //bila user mencari id user yang tidak terdaftar
      if(count($currentchosenuser)==0){
          return redirect('usersadministration')->with('error',"Halaman yang anda cari tidak ditemukan.");
      }
      else
      {

        // return for super admin roles
        if(!empty(Sentinel::getUser()->inRole('super-admin'))){
          if($currentchosenuser['0']->slug!='admin-pusat'){
            return redirect('usersadministration')->with('error',"Halaman yang anda cari tidak ditemukan.");
          } else{
            return $return;
          }
        }

        // return for admin pusat roles
        if(!empty(Sentinel::getUser()->inRole('admin-pusat'))){
          if($currentchosenuser['0']->slug!='admin-provinsi'){
            return redirect('usersadministration')->with('error',"Halaman yang anda cari tidak ditemukan.");
          } else{
            return $return;
          }
        }

        // return for admin provinsi roles
        if(!empty(Sentinel::getUser()->inRole('admin-provinsi'))){
          if(($currentchosenuser['0']->slug=='admin-kabko')&&($currentchosenuser['0']->province_id==Sentinel::getUser()->province_id)){
            return $return;
          } else{
            return redirect('usersadministration')->with('error',"Halaman yang anda cari tidak ditemukan.");
          }
        }

        // return for admin kabko roles
        if(!empty(Sentinel::getUser()->inRole('admin-kabko'))){
          if(($currentchosenuser['0']->slug=='admin-kecamatan')&&($currentchosenuser['0']->kab_id==Sentinel::getUser()->kab_id)){
            return $return;
          } else{
            return redirect('usersadministration')->with('error',"Halaman yang anda cari tidak ditemukan.");
          }
        }

        // return for admin kecamatan roles try to access
        if(!empty(Sentinel::getUser()->inRole('admin-kecamatan'))){
            return redirect('/')->with('error',"Halaman yang anda cari tidak ditemukan.");
        }
      }


    }


    //------------
    //this function has disabled from route declaration(web.php), due to functional requirement.
    //this function is not preventing admin-kecamatan from accessing yet
    public function showedit($id)
    {
      $userprofile = Sentinel::getUser();
      $currentchosenuser = DB::table('users')
                ->join('role_users', 'users.id', '=', 'role_users.user_id')
                ->join('roles', 'role_users.role_id', '=', 'roles.id')
                ->where('users.id', '=', $id)
                ->get();
      $roles = \DB::table('roles')
                ->get(array(
                            'roles.id',
                            'roles.slug',
                            'roles.name',
                            'roles.permissions',
                ));

      // return print($currentchosenuser);
      return view('layout.contents.user.edit', array(
        'userprofile'=>$userprofile,
        'currentchosenuser'=>$currentchosenuser,
        'roles'=>$roles
      ));
    }


    //------------
    //valid till prevent admin-kecamatan from accessing
    public function updatepass($id, Request $request)
    {
      // return for admin kecamatan roles try to access
      if(!empty(Sentinel::getUser()->inRole('admin-kecamatan'))){
          return redirect('/')->with('error',"Halaman yang anda cari tidak ditemukan.");
      }else
      {
        $credentials = Sentinel::findUserById($id);
        if($credentials->email!=$request->email)
        {
          $data['error']="true";
          $data['message']="Email tidak ditemukan.";
          return response()->json($data);
        }else {
          $user = Sentinel::findById($id);
          $user->password = Hash::make($request->password_baru);
          $user->save();
          $data['error']="false";
          $data['message']="Password berhasil diubah.";
          $data['url']=url("usersadministration/show/".$id);
          return response()->json($data);
        }
      }
    }


    //------------
    //this function has disabled from route declaration(web.php), due to functional requirement.
    //this function is not preventing admin-kecamatan from accessing yet
    public function update($id, Request $request)
    {
      $user = DB::table('users')
            ->where('id', $id)
            ->update([
              'first_name' => $request->first_name,
              'last_name' => $request->last_name,
              'username' => $request->username,
              'email' => $request->email,
              'phone_number' => $request->phone_number,
              'mobile_number' => $request->mobile_number,
            ]);
      $user = Sentinel::findById($id);
      $user->touch();

      $role = DB::table('role_users')
            ->where('user_id', $id)
            ->update(['role_id' => $request->role]);


      $data['error']="false";
      $data['message']="Profil berhasil diubah.";
      $data['url']=url("usersadministration/show/".$id);
      return response()->json($data);
      // return redirect('usersadministration/'.$id)->with('message',"Profil berhasil diubah.");
    }

}
