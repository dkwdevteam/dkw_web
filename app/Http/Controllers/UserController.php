<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Sentinel;

use Redirect;

class UserController extends Controller
{


    public function login()
    {
      if (Sentinel::check())
      {
          // User is logged in and assigned to the `$user` variable.
          $userprofile = Sentinel::getUser();
          return view('layout.contents.dashboard')->with('userprofile', $userprofile);
          // return redirect('/');
      }
      else
      {
          // User is not logged in
          return view('authentication/index');
      }
    }



    public function postLogin(Request $request)
    {
      $credentials = [
          'email'    => $request->username,
          'password' => $request->pass,
      ];

      $user = Sentinel::authenticate($credentials);

      if ($user=Sentinel::check())
      {
          // User is logged in and assigned to the `$user` variable.
          $userprofile = Sentinel::getUser();
          return redirect('/')->with('userprofile', $userprofile);
      }
      else
      {
          // User is not logged in
          return Redirect::back()->withErrors(['Username atau password salah.']);
      }
    }

    public function userProfile()
    {
      $userprofile = Sentinel::getUser();
      return view('authentication/show')->with('userprofile', $userprofile);
    }

    public function edit($id)
    {
      $userprofile = Sentinel::getUser();
      return view('authentication/edit')->with('userprofile', $userprofile);
    }

    public function editPassword($id)
    {
      $userprofile = Sentinel::getUser();
      return view('authentication/password')->with('userprofile', $userprofile);
    }

    public function update($id, Request $request){
      $user = Sentinel::findById($id);
              $user->first_name = $request->first_name;
              $user->last_name = $request->last_name;
              $user->username = $request->username;
              $user->email = $request->email;
              $user->phone_number = $request->phone_number;
              $user->mobile_number = $request->mobile_number;
              $user->save();

      return redirect('userprofile')->with('message',"Profil berhasil diubah.");
    }

    public function updatePassword($id, Request $request){
      $credentials = [
      'email'    => $request->email,
      'password' => $request->password_lama,
      ];

      if(!Sentinel::authenticate($credentials))
      {
        $data['error']="true";
        $data['message']="Email dan password lama tidak ditemukan.";
        return response()->json($data);
      }else {
        $user = Sentinel::findById($id);
                $user->password = Hash::make($request->password_baru);
                $user->save();
        $data['error']="false";
        $data['message']="Password berhasil diubah.";
        $data['url']=url("userprofile");
        return response()->json($data);
      }
    }


    public function logout()
    {
      Sentinel::logout();
      return redirect('login');
    }
}
