<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Notifications;

use App\Models\Users;

use App\Models\KecamatanDeposit;
use App\Models\Cities;

use Sentinel;

class NotifikasiController extends Controller
{
	public function index() {
		if (!Sentinel::check()) 
			return view('authentication/login');

		$userprofile = Sentinel::getUser();
		$dataNotif = [];
		
		$user = Users::find($userprofile->id);
		if(isset($user->kec_id) && $user->kec_id != -1){
			$notif = KecamatanDeposit::where('id_kecamatan', $userprofile->kec_id)->where('status','ditolak-kabko')->get();	
			foreach($notif as $nk => $nv){
				$data = (object)[
					'name' => $nv->getKecamatan->name,
					'date' => $nv->cash_date,
					'url' => url('setoran/kabupaten/subdetails/'. $nv->id)
				];
				array_push($dataNotif, $data);
			}
		}
		if(!isset($user->kec_id) && isset($user->kab_id) && $user->kab_id != -1 || $user->kec_id == -1 && isset($user->kab_id) && $user->kab_id != -1){
			$notif = KecamatanDeposit::where('id_city', $userprofile->kab_id)->where('status','diajukan-kecamatan')->get();	
			foreach($notif as $nk => $nv){
				$data = (object)[
					'name' => $nv->getKecamatan->name,
					'date' => $nv->cash_date,
					'url' => url('setoran/kabupaten/subdetails/'. $nv->id)
				];
				array_push($dataNotif, $data);
			}
		}
		if(!isset($user->kec_id) && !isset($user->kab_id) && !isset(($user->provice_id))){
			$list = KecamatanDeposit::where('status','diajukan-kabko')->get();
			$dataNotif = [];
			$kabkolist = [];
			foreach($list as $lk => $lv){
				if(in_array($lv->id_city, $kabkolist)){

				} else {
					$data = (object)[
						'name' => Cities::find($lv->id_city)->name,
						'date' => $lv->cash_date,
						'url' => url('#')
					];
					array_push($kabkolist, $lv->id_city);
					array_push($dataNotif, $data);
				}
			}

		}
		return view('layout.contents.notifikasi',array(
			'userprofile'=>$userprofile,
			'notifikasi'=> $dataNotif
		));
	}
}
