<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Users;
use Sentinel;
use DB;

class UserManagerController extends Controller
{
	public function index(){
		$loginUser = Sentinel::check();
		if(Sentinel::inRole('super-admin')){
			$userlist = Users::all();
			foreach($userlist as $uk => $uv){
				$uu = Sentinel::findById($uv->id);
				
             	if($uu->inRole('super-admin')){
             		unset($userlist[$uk]);
             	}   
			}
			return view('layout.contents.usermanager.index', array(
		        'userprofile'=>$loginUser,
		        'user'=>$userlist
		     ));
		}
		if(Sentinel::inRole('admin-provinsi')){

		}
	}

	public function add(){
		$loginUser = Sentinel::check();
		if($loginUser){
			
		} else {
			return redirect('/');
		}

	}

	public function store(Request $request){

	}

	public function edit(){

	}

	public function update(){
		
	}
}
