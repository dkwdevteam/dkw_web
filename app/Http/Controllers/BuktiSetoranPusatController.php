<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Sentinel;
use Illuminate\Support\Facades\DB;

use App\Models\KecamatanDeposit;
use App\Models\KecamatanDepositDetails;
use App\Models\KecamatanDepositLog;
use App\Models\Districts;
use App\Models\BuktiSetorDetails;
use App\Models\BuktiSetor;

use App\Helpers\NotificationHelper;

class BuktiSetoranPusatController extends Controller
{
    public function index(){
      if (!Sentinel::check())
  		return view('authentication/login');
      $user = Sentinel::check();
  		$data['userprofile'] = $user;
      $data['buktisetor'] = BuktiSetor::all();

      // return response()->json($data);
      return view('layout/contents/buktisetoranpusat/index')->with($data);
    }

    public function details($id){
      if (!Sentinel::check())
  		return view('authentication/login');
      $user = Sentinel::check();
  		$data['userprofile'] = $user;
      $data['buktisetor'] = BuktiSetor::where('id',$id)->first();

      // return response()->json($data);
      return view('layout/contents/buktisetoranpusat/details')->with($data);
    }
}
