<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Sentinel;

use Redirect;

class SetoranController extends Controller
{
	public function setoran() {
		if (!Sentinel::check()) 
			return view('authentication/login');

		$userprofile = Sentinel::getUser();
		return view('layout.contents.setoran.index',array(
			'userprofile'=>$userprofile
		));
	}
}
