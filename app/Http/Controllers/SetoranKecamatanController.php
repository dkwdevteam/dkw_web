<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Sentinel;

use App\Models\KecamatanDeposit;
use App\Models\KecamatanDepositDetails;
use App\Models\KecamatanDepositLog;
use App\Models\Districts;

use App\Helpers\NotificationHelper;

class SetoranKecamatanController extends Controller
{
	public function setoranDbSpKecamatan() {
		if (!Sentinel::check())
			return view('authentication/login');

		$userprofile = Sentinel::getUser();
		return view('layout.contents.setorankecamatan.index',array(
			'userprofile'=>$userprofile
		));
	}


	public function storeDeposit(Request $request){

		$user = Sentinel::check();
		$deposit = new KecamatanDeposit();

		$deposit->id_country = 101;
		$deposit->id_region = $request->id_region;
		$deposit->id_city = $request->id_city;
		$deposit->id_kecamatan = $request->id_kecamatan;
		$deposit->cash_date = $request->cash_date;

		$deposit->cash_month = $request->bulan;
		$deposit->cash_year = $request->tahun;

		$deposit->people_deposits = $request->penghimpun;

		$deposit->cash_date = date('Y-m-d h:i:s');


		$setorDb = self::cleanValue($request->db_total);
		$setorSp = self::cleanValue($request->sp_total);
		$deposit->cash_db = $setorDb;
		$deposit->cash_sp = $setorSp;
		$depototal = $setorDb+$setorSp;

		$db10Persen = (10/100)*$setorDb;
		$dbtotal10 = $setorDb - $db10Persen;

		$db15Persen1 = (15/100)*$dbtotal10;

		$dbtotal151 = $dbtotal10-$db15Persen1;



		$db15persen2 = (15/100)*$dbtotal151;

		$dbtotal152 = $dbtotal151 - $db15persen2;

		$db15persen3 = (15/100)*$dbtotal152;

		$dbtotal153 = $dbtotal152 - $db15persen3;

		$db15persen4 = (15/100)*$dbtotal153;


		$sp15Persen1 = (15/100)*$setorSp;
		$sptotal15 = $setorSp - $sp15Persen1;

		$sp15Persen2 = (15/100)*$sptotal15;

		$sptotal152 = $sptotal15 - $sp15Persen2;

		$sp15Persen3 = (15/100)*$sptotal152;

		$sptotal153 = $sptotal152- $sp15Persen3;

		$sp15Persen4 = (15/100)*$sptotal153;





		$deposit->db_10persen = $db10Persen;
		$deposit->db_15persen_1 = $db15Persen1;
		$deposit->sp_15persen_1 = $sp15Persen1;

		$deposit->db_15persen_2 = $db15persen2;
		$deposit->sp_15persen_2 = $sp15Persen2;

		$deposit->db_15persen_3 = $db15persen3;
		$deposit->sp_15persen_3 = $sp15Persen3;

		$deposit->db_15persen_4 = $db15persen4;
		$deposit->sp_15persen_4 = $sp15Persen4;

		$deposit->cash_descs = $request->description;

		$deposit->status = "diajukan-kecamatan";
		$deposit->save();
		$setordetail = $request->setordetail;
		foreach($setordetail as $sk => $sv){
			$depositdetail = new KecamatanDepositDetails();
			$depositdetail->people_id = $sk;
			$depositdetail->danabox = self::cleanValue($setordetail[$sk]['db']);
			$depositdetail->sp = self::cleanValue($setordetail[$sk]['sp']);
			$depositdetail->deposit_id = $deposit->id;
			$depositdetail->detail = $sv['detail'];
			$depositdetail->save();
		}

		$setorlog = new KecamatanDepositLog();
		$setorlog->kec_id = $request->id_kecamatan;
		$setorlog->user_id = $user->id;
		$setorlog->deposit_id = $deposit->id;
		$kec = Districts::find($request->id_kecamatan);
		$setorlog->deskripsi = "Kecamatan ".$kec->name." telah menyimpan setoran pada tanggal sebesar Rp. ".number_format($depototal - $db10Persen,0,',','.')."";
		$setorlog->save();


		NotificationHelper::sendSetoranKecamatanNotif($request->id_kecamatan, $setorlog->id);

		return redirect('setoran/kecamatan/history');

	}


	public function history(){
		$user = Sentinel::check();
		$kec_id = $user->kec_id;
		$data['setoran'] = KecamatanDeposit::where('id_kecamatan', $kec_id)->orderBy('created_at','desc')->get();
		$data['userprofile'] = $user;
		return view('layout/contents/setorankecamatan/history')->with($data);
	}

	public function details($id){
		$user = Sentinel::check();

		$kecDeposit = KecamatanDeposit::find($id);
		if($kecDeposit){
			if($kecDeposit->id_kecamatan == $user->kec_id){
				$data['setoran'] = $kecDeposit;
				$data['userprofile'] = $user;
				return view('layout/contents/setorankecamatan/details')->with($data);

			} else {
				return redirect('setoran/kecamatan/history');
			}
		} else {
			return redirect('setoran/kecamatan/history');
		}
	}


	static function cleanValue($value){
		$value = str_replace('Rp.', '', $value);
		$value = str_replace('.', '', $value);
		$value = str_replace(' ', '', $value);
		return intval($value);
	}


}
