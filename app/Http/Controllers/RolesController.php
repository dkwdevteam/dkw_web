<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Sentinel;

class RolesController extends Controller
{
  public function index()
  {
    // $userList = Sentinel::getUserRepository()->with('roles')->get();
    $user = Sentinel::getUser();
    $roleLists = DB::table('roles')
              ->orderBy('roles.id', 'desc')
              ->paginate(10);
    $rounds=array("","round-warning","round-danger","round-success","round-primary");

    // return $userList;
    return view('layout.contents.roles.index', array(
      'userprofile'=>$user,
      'roleLists'=>$roleLists,
      'rounds'=>$rounds
    ));
  }

  public function search(Request $request)
  {
    if($request->search == "")
    {
      // $userList = Sentinel::getUserRepository()->with('roles')->get();
      $user = Sentinel::getUser();
      $roleLists = DB::table('roles')
                ->orderBy('roles.id', 'desc')
                ->paginate(10);
      $rounds=array("","round-warning","round-danger","round-success","round-primary");

      // return $userList;
      return view('layout.contents.roles.index', array(
        'userprofile'=>$user,
        'roleLists'=>$roleLists,
        'rounds'=>$rounds
      ));
    }else {
      // $userList = Sentinel::getUserRepository()->with('roles')->get();
      $user = Sentinel::getUser();
      $roleLists = DB::table('roles')
                ->where('roles.slug','LIKE','%'.trim($request->search).'%')
                ->orWhere('roles.name','LIKE','%'.trim($request->search).'%')
                ->orderBy('roles.id', 'desc')
                ->paginate(10);
      $roleLists->appends($request->only('search'));
      $rounds=array("","round-warning","round-danger","round-success","round-primary");

      // return $userList;
      return view('layout.contents.roles.index', array(
        'userprofile'=>$user,
        'roleLists'=>$roleLists,
        'rounds'=>$rounds
      ));
    }
  }

  public function store(Request $request)
  {
    $role = Sentinel::getRoleRepository()->createModel()->create([
    'name' => $request->name,
    'slug' => $request->slug,
    ]);
    $data['error']="false";
    $data['message']="Role baru berhasil disimpan.";
    $data['url']=url("roles");
    return response()->json($data);
  }

  public function create()
  {
    $roles = \DB::table('roles')
              ->get(array(
                          'roles.id',
                          'roles.slug',
                          'roles.name',
                          'roles.permissions',
              ));
    $userprofile = Sentinel::getUser();
    return view('layout.contents.roles.create', array(
      'userprofile'=>$userprofile,
      'roles'=>$roles
    ));
  }

  public function update($id, Request $request)
  {
    $user = Sentinel::findRoleById($id);
            $user->slug = $request->slug;
            $user->name = $request->name;
            $user->save();
    $data['error']="false";
    $data['message']="Role berhasil diubah.";
    $data['url']=url("roles");
    return response()->json($data);
  }

  public function show($id)
  {
    $userprofile = Sentinel::getUser();
    $roleLists = Sentinel::findRoleById($id);
    return view('layout.contents.roles.show', array(
      'userprofile'=>$userprofile,
      'roleLists'=>$roleLists
    ));
  }

  public function destroy($id)
  {
    $cekdata = \DB::table('role_users')
              ->where('role_id',$id)->count();

      if($cekdata==0){
        $list = Sentinel::findRoleById($id);
        $list->delete();
        $data["error"]="false";
        $data["message"]="Data berhasil dihapus.";
      }else {
        $data["error"]="true";
        $data["message"]="Tidak dapat menghapus data.\nData menjadi referensi tabel lain !";
      }
    return response ()->json ( $data );
  }

}
