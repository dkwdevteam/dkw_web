<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Provinces;

use App\Models\People;
use Sentinel;

class JamaahController extends Controller
{
	public function index(){
		$data['userprofile'] = Sentinel::check();
		$data['provinces'] = Provinces::all();
		return view('layout.contents.people.index')->with($data);
	}

	public function insertPeople(Request $request){

		$response['status'] = false;
		$response['data'] = [];
		$response['message'] = "";


		$pple = new People();
		$pple->kec_id = $request->kecamatan;
		$pple->city_id = $request->city;
		$pple->country_id = 101;
		$pple->nama = $request->nama;
		$pple->alamat = $request->alamat;
		$pple->group = $request->group;
		$pple->telp = $request->telp;
		$pple->is_kk = $request->is_kk;

		if($request->is_kk === "0"){
			$pple->kk_id = $request->kk_id;	
		} else {

		}
		

		if($pple->save()){
			$response['status'] = true;
			$response['message'] = "data has been inserted";
			return response()->json($response);
		} else {
			$response['message'] = "Fail to save data";
			return response()->json($response);
		}
	}

	public function editDataPeople(Request $request){
		$response['status'] = false;
		$response['data'] = [];
		$response['message'] = "";

		$id = $request->id;

		$pple = People::where('id', $id)->first();
		if(count($pple) > 0){
			$pple->kec_id = $request->kecamatan;
			$pple->city_id = $request->city;
			$pple->country_id = 101;
			$pple->nama = $request->nama;
			$pple->alamat = $request->alamat;
			$pple->group = $request->group;
			$pple->telp = $request->telp;
			$pple->is_kk = $request->is_kk;

			if($pple->is_kk === "0"){
				$pple->kk_id = $request->kk_id;	
			} else {

			}		
			if($pple->save()){
				$response['status'] = true;
				$response['message'] = "data has been updated";
				$response['data'] = $pple;
				return response()->json($response);
			} else {
				$response['message'] = "Fail to save data";
				return response()->json($response);
			}
		}
	}

	public function loadListKK(Request $request){
		$response['status'] = true;
		$kec_id = $request->kecamatan;
		$city_id = $request->city;

		$people = People::where('kec_id', $kec_id)->where('city_id', $city_id)->where('group', 'bapak')->where('is_kk',1)->where(function($query){
			$query->where('is_deleted','!=',1);
			$query->orWhereNull('is_deleted');
		})->get();

		$response['data'] = $people;
		$response['message'] = "data found!";
		return response()->json($response);
	}


	public function loadDataPeople(Request $request){
		$response['status'] = true;

		$kec_id = $request->kecamatan;
		$city_id= $request->city;
		$people = People::where('kec_id', $kec_id)->where('city_id', $city_id)->where(function($query){
			$query->where('is_deleted','!=',1);
			$query->orWhereNull('is_deleted');
		})->get();
		

		$response['data'] = $people;
		$response['total_data'] = count($people);
		return response()->json($response);
	}
	

	public function getDataPeople(Request $request){
		$response['status'] = false;
		$id = $request->id;
		$data = People::where('id', $id)->first();
		if(count($data) > 0){
			$response['status'] = true;
			$response['data'] = $data;
			return response()->json($response);
		} else {
			$response['message'] = "data not found";
			return response()->json($response);
		}

	}

	public function deletePeople(Request $request){
		$response['status'] = false;
		$id = $request->id;
		$data = People::where('id', $id)->first();
		if(count($data) > 0){
			$data->is_deleted = 1;
			if($data->save()){
				$response['status'] = true;
				return response()->json($response);
			} else {
				$response['message'] = "Unknown error";
				return response()->json($response);
			}
		} else {
			$response['message'] = "data not found!";
			return response()->json($response);
		}
	}


}
