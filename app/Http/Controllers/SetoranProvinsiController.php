<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Sentinel;
use Illuminate\Support\Facades\DB;

use App\Models\KecamatanDeposit;
use App\Models\KecamatanDepositDetails;
use App\Models\KecamatanDepositLog;
use App\Models\Cities;
use App\Models\Districts;

use App\Helpers\NotificationHelper;

class SetoranProvinsiController extends Controller
{
  public function history(){
    if (!Sentinel::check())
		return view('authentication/login');

		$user = Sentinel::check();
    $province_id = $user->province_id;
		$data['setoran'] = DB::table('tbl_kecamatan_deposit')
                     ->select(DB::raw('DISTINCT(cash_month),cash_year,`status`,
                     sum(cash_db-db_10persen-db_15persen_1-db_15persen_2-db_15persen_3)as totalDB
                     ,sum(cash_sp-sp_15persen_1-sp_15persen_2-sp_15persen_3)as totalSP
                     ,sum(cash_db+cash_sp-db_10persen-db_15persen_1-db_15persen_2-db_15persen_3-sp_15persen_1-sp_15persen_2-sp_15persen_3)as setoran'))
                     ->where('id_region', $province_id)
                     ->whereIn('status', ['diajukan-kabko', 'diterima-pusat', 'ditolak-pusat'])
                     ->groupBy('cash_month','cash_year','status')
                     ->get();

		// KecamatanDeposit::where('id_city', $kab_id)->where('status','diajukan-kabko')->orWhere('status','diterima-pusat')->orWhere('status','ditolak-pusat')->orderBy('created_at','desc')->get();
		$data['userprofile'] = $user;
		return view('layout.contents.setoranprovinsi.history')->with($data);
  }



  public function rekapitulasi(){
    if (!Sentinel::check())
		return view('authentication/login');

		$user = Sentinel::check();
    $province_id = $user->province_id;
		$data['setoran'] = DB::table('tbl_kecamatan_deposit')
                     ->select(DB::raw('DISTINCT(cash_month),cash_year,`status`,sum(cash_db+cash_sp-db_10persen-db_15persen_1-db_15persen_2-sp_15persen_1-sp_15persen_2)as setoran'))
                     ->where('id_region', $province_id)
                     ->whereIn('status', ['diajukan-kabko', 'diterima-pusat', 'ditolak-pusat'])
                     ->groupBy('cash_month','cash_year','status')
                     ->get();


		// KecamatanDeposit::where('id_city', $kab_id)->where('status','diajukan-kabko')->orWhere('status','diterima-pusat')->orWhere('status','ditolak-pusat')->orderBy('created_at','desc')->get();
		$data['userprofile'] = $user;
		return view('layout.contents.setoranprovinsi.rekapitulasi')->with($data);
  }

  public function details($id_month,$id_year,$id_status){
    $user = Sentinel::check();
    $id_region = $user->province_id;
    $data['id_month']=$id_month;
    $data['id_year']=$id_year;
    $data['id_status']=$id_status;
    $data['id_region']=$id_region;
    $data['setoran'] = DB::table('tbl_kecamatan_deposit')
                ->select(DB::raw('id_city,sum(cash_db)as scash_db,sum(cash_sp)as scash_sp,sum(db_10persen)as sdb_10persen,sum(db_15persen_1)as sdb_15persen_1,sum(db_15persen_2)as sdb_15persen_2,sum(db_15persen_3)as sdb_15persen_3,sum(db_15persen_4)as sdb_15persen_4,sum(sp_15persen_1)as ssp_15persen_1,sum(sp_15persen_2)as ssp_15persen_2,sum(sp_15persen_3)as ssp_15persen_3,sum(sp_15persen_4)as ssp_15persen_4'))
                ->where('id_region',$id_region)
                ->where('cash_month',$id_month)
                ->where('cash_year',$id_year)
                ->where('status',$id_status)
                ->groupBy('id_city')
                ->get();
    $data['userprofile'] = $user;
    // return response()->json($data);
    return view('layout/contents/setoranprovinsi/details')->with($data);
  }

  public function subdetails($id_month,$id_year,$id_status,$id_city){
    $user = Sentinel::check();
    $data['id_month']=$id_month;
    $data['id_year']=$id_year;
    $data['id_status']=$id_status;
    $id_region = $user->province_id;
    $data['id_region']=$id_region;
    $data['id_city']=$id_city;
    $cityDeposit = KecamatanDeposit::where('cash_month',$id_month)
                ->where('cash_year',$id_year)
                ->where('status',$id_status)
                ->where('id_city',$id_city)
                ->get();
    if($cityDeposit){
      if(Cities::where('id',$id_city)->first()->province_id == $user->province_id){
        $data['setoran'] = $cityDeposit;
        $data['userprofile'] = $user;
        // return response()->json($data);
        return view('layout/contents/setoranprovinsi/subdetails')->with($data);

      } else {
        return redirect('setoran/provinsi/details/'.$id_month.'/'.$id_year.'/'.$id_status);
      }
    } else {
      return redirect('setoran/provinsi/details/'.$id_month.'/'.$id_year.'/'.$id_status);
    }
  }

  public function subdetails_kecamatan($id_month,$id_year,$id_status,$id_city,$id){
    $user = Sentinel::check();
    $data['id_month']=$id_month;
    $data['id_year']=$id_year;
    $data['id_status']=$id_status;
    $data['id_city']=$id_city;
    $kecDeposit = KecamatanDeposit::find($id);
    if($kecDeposit){
      if($kecDeposit->id_region == $user->province_id){
        $data['setoran'] = $kecDeposit;
        $data['userprofile'] = $user;
        // return response()->json($data);
        return view('layout/contents/setoranprovinsi/subdetails_kecamatan')->with($data);

			} else {
        return redirect('setoran/provinsi/subdetails/'.$id_month.'/'.$id_year.'/'.$id_status.'/'.$id_city);
			}
    } else {
      return redirect('setoran/provinsi/subdetails/'.$id_month.'/'.$id_year.'/'.$id_status.'/'.$id_city);
    }
  }

}
