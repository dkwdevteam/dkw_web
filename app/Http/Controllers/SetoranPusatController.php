<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Sentinel;
use Illuminate\Support\Facades\DB;

use App\Models\KecamatanDeposit;
use App\Models\KecamatanDepositDetails;
use App\Models\KecamatanDepositLog;
use App\Models\Districts;

use App\Helpers\NotificationHelper;

class SetoranPusatController extends Controller
{
    public function history()
    {
      if (!Sentinel::check())
  		return view('authentication/login');

  		$user = Sentinel::check();
  		$data['setoran'] = DB::table('tbl_kecamatan_deposit')
                       ->select(DB::raw('DISTINCT(cash_month),cash_year,`status`,
                       sum(cash_db-db_10persen-db_15persen_1-db_15persen_2-db_15persen_3-db_15persen_4)as totalDB,
                       sum(cash_sp-sp_15persen_1-sp_15persen_2-sp_15persen_3-sp_15persen_4)as totalSP,
                       sum(cash_db+cash_sp-db_10persen-db_15persen_1-db_15persen_2-db_15persen_3-db_15persen_4-sp_15persen_1-sp_15persen_2-sp_15persen_3-sp_15persen_4)as setoran'))
                       ->whereIn('status', ['diajukan-kabko', 'diterima-pusat', 'ditolak-pusat'])
                       ->groupBy('cash_month','cash_year','status')
                       ->get();

      $data['userprofile'] = $user;
  		return view('layout.contents.setoranpusat.history')->with($data);
    }

    public function details($id_month,$id_year,$id_status){
  		$user = Sentinel::check();
      $data['id_month']=$id_month;
      $data['id_year']=$id_year;
      $data['id_status']=$id_status;
  		$data['setoran'] = DB::table('tbl_kecamatan_deposit')
                  ->select(DB::raw('id_region,sum(cash_db)as scash_db,sum(cash_sp)as scash_sp,sum(db_10persen)as sdb_10persen,sum(db_15persen_1)as sdb_15persen_1,sum(db_15persen_2)as sdb_15persen_2,sum(db_15persen_3)as sdb_15persen_3,sum(db_15persen_4)as sdb_15persen_4,sum(sp_15persen_1)as ssp_15persen_1,sum(sp_15persen_2)as ssp_15persen_2,sum(sp_15persen_3)as ssp_15persen_3,sum(sp_15persen_4)as ssp_15persen_4'))
                  ->where('cash_month',$id_month)
                  ->where('cash_year',$id_year)
  								->where('status',$id_status)
                  ->groupBy('id_region')
  								->get();
  		$data['userprofile'] = $user;
  		// return response()->json($data);
  		return view('layout/contents/setoranpusat/details')->with($data);
  	}

    public function subdetails($id_month,$id_year,$id_status,$id_region){
      $user = Sentinel::check();
      $data['id_month']=$id_month;
      $data['id_year']=$id_year;
      $data['id_status']=$id_status;
      $data['id_region']=$id_region;
      $data['setoran'] = DB::table('tbl_kecamatan_deposit')
                  ->select(DB::raw('id_city,sum(cash_db)as scash_db,sum(cash_sp)as scash_sp,sum(db_10persen)as sdb_10persen,sum(db_15persen_1)as sdb_15persen_1,sum(db_15persen_2)as sdb_15persen_2,sum(db_15persen_3)as sdb_15persen_3,sum(db_15persen_4)as sdb_15persen_4,sum(sp_15persen_1)as ssp_15persen_1,sum(sp_15persen_2)as ssp_15persen_2,sum(sp_15persen_3)as ssp_15persen_3,sum(sp_15persen_4)as ssp_15persen_4'))
                  ->where('id_region',$id_region)
                  ->where('cash_month',$id_month)
                  ->where('cash_year',$id_year)
                  ->where('status',$id_status)
                  ->groupBy('id_city')
                  ->get();
      $data['userprofile'] = $user;
      // return response()->json($data);
      return view('layout/contents/setoranpusat/subdetails')->with($data);
    }

    public function subdetails_kabko($id_month,$id_year,$id_status,$id_region,$id_city){
      $user = Sentinel::check();
      $data['id_month']=$id_month;
      $data['id_year']=$id_year;
      $data['id_status']=$id_status;
      $data['id_region']=$id_region;
      $data['id_city']=$id_city;
      $data['setoran'] = KecamatanDeposit::where('cash_month',$id_month)
  								->where('cash_year',$id_year)
  								->where('status',$id_status)
  								->where('id_city',$id_city)
  								->get();
      $data['userprofile'] = $user;
      // return response()->json($data);
      return view('layout/contents/setoranpusat/subdetails_kabko')->with($data);
    }

    public function subdetails_kecamatan($id_month,$id_year,$id_status,$id_region,$id_city,$id){
      $user = Sentinel::check();
      $data['id_month']=$id_month;
      $data['id_year']=$id_year;
      $data['id_status']=$id_status;
      $data['id_region']=$id_region;
      $data['id_city']=$id_city;
  		$kecDeposit = KecamatanDeposit::find($id);
  		if($kecDeposit){
  				$data['setoran'] = $kecDeposit;
  				$data['userprofile'] = $user;
          // return response()->json($data);
          return view('layout/contents/setoranpusat/subdetails_kecamatan')->with($data);
  		} else {
  			return redirect('setoran/setoranpusat/subdetails2/'.$id_month.'/'.$id_year.'/'.$id_status.'/'.$id_region.'/'.$id_city);
  		}
    }
}
