<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Sentinel;
use Illuminate\Support\Facades\DB;

use App\Models\KecamatanDeposit;
use App\Models\KecamatanDepositDetails;
use App\Models\KecamatanDepositLog;
use App\Models\Districts;
use App\Models\BuktiSetor;
use App\Models\BuktiSetorDetails;
use App\Models\Users;

use App\Helpers\NotificationHelper;
use App\Helpers\ImageHelper;

class SetoranKabupatenController extends Controller
{
	public function setoranDbSpKabupaten() {
		if (!Sentinel::check())
			return view('authentication/login');

		$user = Sentinel::check();
		$data['userprofile'] = $user;
		$userprofile = Sentinel::getUser();
		return view('layout.contents.setorankabko.index')->with($data);;
	}

	public function storeDeposit(Request $request){
		$requestData = $request->all();
		$deposit_list = KecamatanDeposit::where('id_city', $request->id_city)
										->where('cash_month', $request->bulan)
										->where('cash_year', $request->tahun)
										->where('status', 'diterima-kabko')->get();
		if(isset($requestData['bukti'])){
			$imageData = (object)[
				'path' => 'bukti_setor/',
				'uniqid' => 'bs',
				'image' => $requestData['bukti'],
			];
			$imagePath = ImageHelper::uploadImage($imageData);
			if($imagePath){
				$buktiSetor = new BuktiSetor();
				$buktiSetor->id_city = $request->id_city;
				$buktiSetor->cash_month = $request->bulan;
				$buktiSetor->cash_year = $request->tahun;
				$buktiSetor->photo = $imagePath;
				$buktiSetor->save();
				foreach($deposit_list as $dk => $dv){
					$buktiDetail = new BuktiSetorDetails();
					$buktiDetail->bukti_id = $buktiSetor->id;
					$buktiDetail->deposit_id = $dv->id;
					$buktiDetail->save();
				}
			}
		}

		KecamatanDeposit::where('id_city',	$request->id_city)
					->where('cash_month',	$request->bulan)
					->where('cash_year',	$request->tahun)
					->where('status', 'diterima-kabko')
					->update([
						'status' => 'diajukan-kabko',
					]);

		return redirect('setoran/kabupaten/history');
	}

	public function preStoreDeposit(Request $request){
		$user = Sentinel::check();
		$data['userprofile'] = $user;
		$data['cash_month'] = $request->bulan;
		$data['cash_year'] = $request->tahun;
		$data['approved_cash'] = KecamatanDeposit::where('id_city',	$request->id_city)->where('cash_month',	$request->bulan)->where('cash_year',	$request->tahun)->where('status', 'diterima-kabko')->get();
		return view('layout.contents.setorankabko.prestore')->with($data);;
	}

	public function history(){
		if (!Sentinel::check())
		return view('authentication/login');

		$user = Sentinel::check();
		$kab_id = Users::find(Sentinel::check()->id)->getCities->id;
		$data['this_city']=Users::find(Sentinel::check()->id)->getCities->name;
		$data['setoran'] = DB::table('tbl_kecamatan_deposit')
                     ->select(DB::raw('DISTINCT(cash_month),cash_year,`status`,
										 sum(cash_db-db_10persen-db_15persen_1-db_15persen_2)as totalDB,
										 sum(cash_sp-sp_15persen_1-sp_15persen_2)as totalSP,
										 sum(cash_db+cash_sp-db_10persen-db_15persen_1-db_15persen_2-sp_15persen_1-sp_15persen_2)as setoran'))
                     ->where('id_city', $kab_id)
										 ->whereIn('status', ['diajukan-kabko', 'diterima-pusat', 'ditolak-pusat'])
                     ->groupBy('cash_month','cash_year','status')
                     ->get();

		$data['userprofile'] = $user;
		return view('layout.contents.setorankabko.history')->with($data);
	}

	public function details($id_month,$id_year,$id_status){
		$user = Sentinel::check();
		$kab = $user->kab_id;
		$data['setoran'] = KecamatanDeposit::where('cash_month',$id_month)
								->where('cash_year',$id_year)
								->where('status',$id_status)
								->where('id_city',$kab)
								->get();
		$data['userprofile'] = $user;
		// return response()->json($data);
		return view('layout/contents/setorankabko/details')->with($data);
	}

	public function subdetails($id){
		$user = Sentinel::check();

		$kecDeposit = KecamatanDeposit::find($id);
		if($kecDeposit){
			if($kecDeposit->id_city == $user->kab_id){
				$data['setoran'] = $kecDeposit;
				$data['userprofile'] = $user;
				return view('layout/contents/setorankabko/subdetails')->with($data);

			} else {
				return redirect('setoran/kabupaten/history');
			}
		} else {
			return redirect('setoran/kabupaten/history');
		}
	}

	public function acceptSetoran($id){
		$user = Sentinel::check();

		$kecDeposit = KecamatanDeposit::find($id);
		if($kecDeposit){
			if($kecDeposit->id_city == $user->kab_id){
				$kecDeposit->status = "diterima-kabko";
				$kecDeposit->save();
				$kecamatan = $kecDeposit->getKecamatan->name;
				$kabupaten = $kecDeposit->getKabko->name;
				$depLog = new KecamatanDepositLog();
				$depLog->kec_id = $kecDeposit->id_kecamatan;
				$depLog->user_id = $user->id;
				$depLog->deposit_id = $kecDeposit->id;
				$depLog->deskripsi = "Setoran kecamatan ".$kecamatan." telah diterima oleh kabupaten ".$kabupaten." pada tanggal ". date('d M Y', strtotime('now'));
				$depLog->save();
				return redirect('setoran/kabupaten/history');
			} else {
				return redirect('setoran/kabupaten/history');
			}
		} else {
			return redirect('setoran/kabupaten/history');
		}
		
	}
}
