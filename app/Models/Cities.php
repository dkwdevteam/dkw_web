<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    //
    protected $fillable = [''];
    protected $table = 'tbl_regencies';

    public function getProvince(){
    	return $this->belongsTo('App\Models\Provinces', 'province_id');
    }
}
