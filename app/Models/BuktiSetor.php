<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuktiSetor extends Model
{
  protected $fillable = [''];
  protected $table = 'tbl_bukti_setor';


  public function getDetails(){
    return $this->hasMany('App\Models\BuktiSetorDetails', 'bukti_id');
  }

  public function getKabko(){
    return $this->belongsTo('App\Models\Cities', 'id_city');
  }
}
