<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Districts extends Model
{
    //
    protected $fillable = [''];
    protected $table = 'tbl_districts';


    public function getCities(){
    	return $this->belongsTo('App\Models\Cities', 'regency_id');
    }
}
