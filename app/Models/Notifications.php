<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    //
    protected $fillable = [''];
    protected $table = 'tbl_notification';


    public function getDeposit(){
    	return $this->belongsTo('App\Models\KecamatanDeposit', 'deposit_id');
    }
}
