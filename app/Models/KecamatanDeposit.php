<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KecamatanDeposit extends Model
{
    //
    protected $fillable = [''];
    protected $table = 'tbl_kecamatan_deposit';


    public function getDetails(){
    	return $this->hasMany('App\Models\KecamatanDepositDetails', 'deposit_id');
    }

    public function getBukti(){
    	return $this->hasOne('App\Models\BuktiSetorDetails', 'deposit_id');
    }

    public function getKecamatan(){
    	return $this->belongsTo('App\Models\Districts', 'id_kecamatan');
    }

    public function getKabko(){
      return $this->belongsTo('App\Models\Cities', 'id_city');
    }

    public function getProvinsi(){
      return $this->belongsTo('App\Models\Provinces', 'id_region');
    }
}
