<?php

namespace App\Models;

use Cartalyst\Sentinel\Users\EloquentUser as SentinelUser;

class Users extends SentinelUser
{
    protected $fillable =[
    	'email',
    	'username',
    	'password',
    	'last_name',
      'first_name',
      'phone_number',
    	'mobile_number',
      'permissions',
      'province_id',
      'kab_id',
    	'kec_id',
    ];
    protected $loginNames = ['username','email'];

    public function getKecamatan(){
        return $this->belongsTo('App\Models\Districts', 'kec_id');
    }

    public function getCities(){
        return $this->belongsTo('App\Models\Cities', 'kab_id');
    }

    public function getProvince(){
        return $this->belongsTo('App\Models\Provinces', 'province_id');
    }

    public function getRole(){

    }

}
