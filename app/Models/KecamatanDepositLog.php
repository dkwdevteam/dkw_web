<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KecamatanDepositLog extends Model
{
    //
    protected $fillable = [''];
    protected $table = 'tbl_kecamatan_deposit_log';


    public function getDeposit(){
    	return $this->belongsTo('App\Models\KecamatanDeposit', 'deposit_id');
    }
}
