<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuktiSetorDetails extends Model
{
  protected $fillable = [''];
  protected $table = 'tbl_bukti_setor_details';

  public function getFoto(){
  	return $this->belongsTo('App\Models\BuktiSetor', 'bukti_id');
  }

  public function getDetails(){
    return $this->belongsTo('App\Models\KecamatanDeposit', 'deposit_id');
  }

}
