<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KecamatanDepositDetails extends Model
{
    //
    protected $fillable = [''];
    protected $table = 'tbl_kecamatan_deposit_details';


    public function getDeposit(){
    	return $this->belongsTo('App\Models\KecamatanDeposit', 'deposit_id');
    }

    public function getPeople(){
    	return $this->belongsTo('App\Models\People', 'people_id');
    }
}
