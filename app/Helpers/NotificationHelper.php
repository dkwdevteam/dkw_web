<?php

namespace App\Helpers;

use App\Models\Users;
use App\Models\KecamatanDeposit;
use App\Models\KecamatanDepositDetails;
use App\Models\KecamatanDepositLog;
use App\Models\Notifications;

use App\Models\Districts;

use Sentinel;
class NotificationHelper {

	public static function sendNotification($user_id, $deposit_id){

	}

	public static function sendSetoranKecamatanNotif($kec_id, $deposit_id){
		$kec = Districts::find($kec_id);
		$kab_id = $kec->regency_id;
		$users = Users::where('kab_id', $kab_id)->get();
		foreach($users as $uk => $uv){
			$notif = new Notifications();
			$notif->user_id = $uv->id;
			$notif->deposit_id = $deposit_id;
			$notif->url = "";
			$notif->message = "1 setoran kecamatan baru.";
			$notif->save();
		}
	}

	public static function setoranKecamatanRejected($deposit_id){

	}

	public static function sendSetoranKabupatenNotif($kab_id, $deposit_id){

	}

	public static function sendNotificationKecamatan($kec_id, $message){

	}
	
}