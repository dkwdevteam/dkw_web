<!DOCTYPE html>
<html lang="en">
<head>
	<title>Log in to your DKW Web App Account</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<!-- <link rel="icon" type="image/png" href="images/icons/favicon.ico"/> -->
  <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}" >
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css"> -->
  {!!Html::style('auth_assets/vendor/bootstrap/css/bootstrap.min.css')!!}
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css"> -->
  {!!Html::style('auth_assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css')!!}
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css"> -->
  {!!Html::style('auth_assets/fonts/iconic/css/material-design-iconic-font.min.css')!!}
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css"> -->
  {!!Html::style('auth_assets/vendor/animate/animate.css')!!}
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css"> -->
  {!!Html::style('auth_assets/vendor/css-hamburgers/hamburgers.min.css')!!}
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css"> -->
  {!!Html::style('auth_assets/vendor/animsition/css/animsition.min.css')!!}
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css"> -->
  {!!Html::style('auth_assets/vendor/select2/select2.min.css')!!}
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css"> -->
  {!!Html::style('auth_assets/vendor/daterangepicker/daterangepicker.css')!!}
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="css/util.css"> -->
  {!!Html::style('auth_assets/css/util.css')!!}
	<!-- <link rel="stylesheet" type="text/css" href="css/main.css"> -->
  {!!Html::style('auth_assets/css/main.css')!!}
<!--===============================================================================================-->
</head>
<body>

	<div class="limiter">
		<div class="container-login100" style="background-image: url('{{ asset('auth_assets/images/bg-01.jpg')}}')">
			<div class="wrap-login100">
				<form class="login100-form validate-form" action="{{url('login')}}" method="POST" id="formLogin">
          {{csrf_field()}}
					<span class="login100-form-logo">
						<i class="zmdi zmdi-landscape"></i>
					</span>

					<span class="login100-form-title p-b-34 p-t-27">
						Log in
					</span>

          @if($errors->any())
          <div class="alert alert-warning" style="margin-bottom: 30px;text-align: center;">
               <i class="fa fa-warning"></i>
              {{$errors->first()}}
          </div>
          @endif

					<div class="wrap-input100 validate-input" data-validate = "Enter username">
						<input class="input100" type="text" name="username" placeholder="Username">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="pass" placeholder="Password">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>

					<div class="contact100-form-checkbox">
						<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
						<label class="label-checkbox100" for="ckb1">
							Remember me
						</label>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit" form="formLogin" value="Submit">
							Login
						</button>
					</div>

					<div class="text-center p-t-90">
						<a class="txt1" href="#">
							Forgot Password?
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>


	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<!-- <script src="vendor/jquery/jquery-3.2.1.min.js"></script> -->
  {!!Html::script('auth_assets/vendor/jquery/jquery-3.2.1.min.js')!!}
<!--===============================================================================================-->
	<!-- <script src="vendor/animsition/js/animsition.min.js"></script> -->
  {!!Html::script('auth_assets/vendor/animsition/js/animsition.min.js')!!}
<!--===============================================================================================-->
	<!-- <script src="vendor/bootstrap/js/popper.js"></script> -->
  {!!Html::script('auth_assets/vendor/bootstrap/js/popper.js')!!}
	<!-- <script src="vendor/bootstrap/js/bootstrap.min.js"></script> -->
  {!!Html::script('auth_assets/vendor/bootstrap/js/bootstrap.min.js')!!}
<!--===============================================================================================-->
	<!-- <script src="vendor/select2/select2.min.js"></script> -->
  {!!Html::script('auth_assets/vendor/select2/select2.min.js')!!}
<!--===============================================================================================-->
	<!-- <script src="vendor/daterangepicker/moment.min.js"></script> -->
  {!!Html::script('auth_assets/vendor/daterangepicker/moment.min.js')!!}
	<!-- <script src="vendor/daterangepicker/daterangepicker.js"></script> -->
  {!!Html::script('auth_assets/vendor/daterangepicker/daterangepicker.js')!!}
<!--===============================================================================================-->
	<!-- <script src="vendor/countdowntime/countdowntime.js"></script> -->
  {!!Html::script('auth_assets/vendor/countdowntime/countdowntime.js')!!}
<!--===============================================================================================-->
	<!-- <script src="js/main.js"></script> -->
  {!!Html::script('auth_assets/js/main.js')!!}

</body>
</html>
