@extends('layout.material-main')
@section('content')

<h1>Ubah Profil</h1>

    <div class="row">
                        <!-- Column -->
                        <div class="col-lg-4 col-xlg-3 col-md-5">
                            <div class="card">
                                <div class="card-block">
                                  <center class="m-t-30"> <img src="{{url('auth_assets\images\user.png')}}" class="img-circle" width="150">
                                      <h4 class="card-title m-t-10">
                                        @if((!$userprofile->first_name)||(!$userprofile->last_name))
                                        John Doe
                                        @else
                                        {{$userprofile->first_name}}
                                        {{$userprofile->last_name}}
                                        @endif
                                      </h4>
                                      <h6 class="card-subtitle">{{$userprofile->email}}</h6>
                                      <div class="row text-center justify-content-md-center">
                                          <div class="col-4"><a href="#" class="link"><i class="icon-bell"></i> <font class="font-medium">0</font></a></div>
                                          <div class="col-4"><a href="#" class="link"><i class="icon-flag"></i> <font class="font-medium">0</font></a></div>
                                      </div>
                                  </center>
                                </div>
                            </div>
                            @if(session()->has('message'))
                            <div class="alert alert-warning  alert-dismissible ">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
                              {{session()->get('message')}}
                            </div>
                            @endif
                        </div>
                        <!-- Column -->
                        <!-- Column -->
                        <div class="col-lg-8 col-xlg-9 col-md-7">
                            <div class="card">
                                <div class="card-block">
                                    <form class="form-horizontal form-material" action="{{url('userprofile/'.$userprofile->id)}}" method="POST">
                                      <input type="hidden" name="_method" value="PATCH">
                                      {{csrf_field()}}
                                        <div class="form-group">
                                            <label class="col-md-12">First Name</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="..." class="form-control form-control-line" value="{{$userprofile->first_name}}" id="first_name" name="first_name" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Last Name</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="..." class="form-control form-control-line" value="{{$userprofile->last_name}}" id="last_name" name="last_name" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Username</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="..." class="form-control form-control-line" value="{{$userprofile->username}}" id="username" name="username" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="example-email" class="col-md-12">Email</label>
                                            <div class="col-md-12">
                                                <input type="email" placeholder="..." class="form-control form-control-line" name="email" id="email" value="{{$userprofile->email}}" required email>
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label class="col-md-12">Password</label>
                                            <div class="col-md-12">
                                                <input type="password" value="password" class="form-control form-control-line">
                                            </div>
                                        </div> -->
                                        <div class="form-group">
                                            <label class="col-md-12">Phone No</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="..." class="form-control form-control-line" value="{{$userprofile->phone_number}}" id="phone_number" name="phone_number">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Mobile No</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="..." class="form-control form-control-line" value="{{$userprofile->mobile_number}}" id="mobile_number" name="mobile_number">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button class="btn btn-success  mdi mdi-content-save">Update Profil</button>
                                                <a href="{{URL::previous()}} " class="btn btn-warning mdi mdi-arrow-left-bold">Kembali</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                    </div>


@endsection

@section('page-script')

@endsection
