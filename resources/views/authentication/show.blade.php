@extends('layout.material-main')
@section('content')

<h1>Profil Saya</h1>
<div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-block">
                                <center class="m-t-30"> <img src="{{url('auth_assets\images\user.png')}}" class="img-circle" width="150">
                                    <h4 class="card-title m-t-10">
                                      @if((!$userprofile->first_name)||(!$userprofile->last_name))
                                      John Doe
                                      @else
                                      {{$userprofile->first_name}}
                                      {{$userprofile->last_name}}
                                      @endif
                                    </h4>
                                    <h6 class="card-subtitle">{{$userprofile->email}}</h6>
                                    <div class="row text-center justify-content-md-center">
                                        <div class="col-4"><a href="#" class="link"><i class="icon-bell"></i> <font class="font-medium">0</font></a></div>
                                        <div class="col-4"><a href="#" class="link"><i class="icon-flag"></i> <font class="font-medium">0</font></a></div>
                                    </div>
                                </center>
                            </div>
                        </div>
                        @if(session()->has('message'))
                        <div class="alert alert-success  alert-dismissible ">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <i class="mdi mdi-information"></i><strong> Informasi!</strong><br>
                          {{session()->get('message')}}
                        </div>
                        @endif
                        @if(isset($_GET['message']))
                        <div class="alert alert-success  alert-dismissible ">
                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                          <i class="mdi mdi-information"></i><strong> Informasi!</strong><br>
                          {{$_GET['message']}}
                        </div>
                        @endif
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <div class="card-block">
                              <div class="panel panel-info">
                                  <div class="panel-heading" style="padding-bottom:10px">
                                      <h3 class="">

                                      </h3>
                                    </div>
                                  <div class="panel-body">
                                      <div class="row">
                                        <!-- <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="{{url('auth_assets\images\user.png')}}" class="img-circle img-responsive"> -->
                                        </div>

                                        <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
                                          <dl>
                                            <dt>DEPARTMENT:</dt>
                                            <dd>Administrator</dd>
                                            <dt>HIRE DATE</dt>
                                            <dd>11/12/2013</dd>
                                            <dt>DATE OF BIRTH</dt>
                                               <dd>11/12/2013</dd>
                                            <dt>GENDER</dt>
                                            <dd>Male</dd>
                                          </dl>
                                        </div>-->
                                        <div class=" col-md-9 col-lg-9 ">
                                          <table class="table table-user-information">
                                            <tbody>
                                              <tr>
                                                <td>First Name</td>
                                                <td>
                                                  @if(!$userprofile->first_name)
                                                  John
                                                  @else
                                                  {{$userprofile->first_name}}
                                                  @endif
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>Last Name</td>
                                                <td>
                                                  @if(!$userprofile->last_name)
                                                  Doe
                                                  @else
                                                  {{$userprofile->last_name}}
                                                  @endif
                                                </td>
                                              </tr>
                                              <tr>
                                                <td>Username</td>
                                                <td>{{$userprofile->username}}</td>
                                              </tr>

                                          <tr>
                                              <tr>
                                                <td>Email</td>
                                                <td><a href="mailto:{{$userprofile->email}}">{{$userprofile->email}}</a></td>
                                              </tr>
                                                <td>Phone Number</td>
                                                <td><i class="mdi mdi-phone-classic"></i> {{$userprofile->phone_number}}<br><br><i class="mdi mdi-cellphone-iphone"></i> {{$userprofile->mobile_number}}
                                                </td>
                                          </tr>

                                            </tbody>
                                          </table>

                                          <!-- <a href="#" class="btn btn-primary">My Sales Performance</a> -->
                                          <!-- <a href="#" class="btn btn-primary">Team Sales Performance</a> -->
                                        </div>
                                      </div>
                                    </div>
                                  <div class="panel-footer">
                                            <!-- <a data-original-title="Broadcast Message" data-toggle="tooltip" data-animation="false"type="button" class="btn btn-sm btn-primary"><i class="mdi mdi-pencil-box-outline"></i></a> -->
                                            <span class="pull-right">
                                              <a href="{{url('userpassword/'.$userprofile->id)}}" data-original-title="Ubah Password" data-toggle="tooltip" data-animation="false"type="button" class="btn btn-sm btn-success"><i class="mdi mdi-account-key"></i> Ubah Password</a>
                                              <a href="{{url('userprofile/'.$userprofile->id)}}" data-original-title="Ubah Profil" data-toggle="tooltip" data-animation="false"type="button" class="btn btn-sm btn-success"><i class="mdi mdi-pencil-box-outline"></i> Ubah Profil</a>
                                                <!-- <a data-original-title="Remove this user" data-toggle="tooltip" data-animation="false"type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a> -->
                                            </span>
                                     </div>
                              </div>













                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>



@endsection

@section('page-script')

@endsection
