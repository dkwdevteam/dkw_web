@extends('layout.material-main')
@section('content')

<h1>Ubah Profil</h1>

    <div class="row">
                        <!-- Column -->
                        <div class="col-lg-4 col-xlg-3 col-md-5">
                            <div class="card">
                                <div class="card-block">
                                  <center class="m-t-30"> <img src="{{url('auth_assets\images\user.png')}}" class="img-circle" width="150">
                                      <h4 class="card-title m-t-10">
                                        @if((!$userprofile->first_name)||(!$userprofile->last_name))
                                        John Doe
                                        @else
                                        {{$userprofile->first_name}}
                                        {{$userprofile->last_name}}
                                        @endif
                                      </h4>
                                      <h6 class="card-subtitle">{{$userprofile->email}}</h6>
                                      <div class="row text-center justify-content-md-center">
                                          <div class="col-4"><a href="#" class="link"><i class="icon-bell"></i> <font class="font-medium">0</font></a></div>
                                          <div class="col-4"><a href="#" class="link"><i class="icon-flag"></i> <font class="font-medium">0</font></a></div>
                                      </div>
                                  </center>
                                </div>
                            </div>
                            @if(session()->has('message'))
                            <div class="alert alert-warning  alert-dismissible ">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
                              {{session()->get('message')}}
                            </div>
                            @endif
                            <div class="alert alert-warning  alert-dismissible " id="errorDiv" style="display:none;">
                              <!-- <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> -->
                              <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
                              <p id="errorDivp"></p>
                            </div>
                        </div>
                        <!-- Column -->
                        <!-- Column -->
                        <div class="col-lg-8 col-xlg-9 col-md-7">
                            <div class="card">
                                <div class="card-block">
                                    <form class="form-horizontal form-material">
                                      <input type="hidden" name="_method" value="PATCH">
                                        <div class="form-group">
                                            <label for="example-email" class="col-md-12">Email</label>
                                            <div class="col-md-12">
                                                <input type="email" placeholder="..." class="form-control form-control-line" name="email" id="email" value="{{$userprofile->email}}" required email>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Password Lama</label>
                                            <div class="col-md-12">
                                                <input type="password" placeholder="..." value="" class="form-control form-control-line" id="password_lama" name="password_lama">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Password Baru</label>
                                            <div class="col-md-12">
                                                <input type="password" placeholder="..." value="" class="form-control form-control-line" id="password_baru" name="password_baru">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Ulangi Password Baru</label>
                                            <div class="col-md-12">
                                                <input type="password" placeholder="..." value="" class="form-control form-control-line" id="password_baru_retype" name="password_baru_retype">
                                            </div>
                                        </div>
                                        <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
                                        <input type="hidden" id="userid" name="userid" value="{{$userprofile->id}}">
                                        <input type="hidden" id="linked" name="linked" value="{{url('userpassword')}}">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <a class="btn btn-success mdi mdi-content-save" onclick="updateresult()" style="color: white;">Update Password</a>
                                                <a href="{{URL::previous()}} " class="btn btn-warning mdi mdi-arrow-left-bold">Kembali</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- Column -->
                    </div>


@endsection

@section('page-script')
<script>
    //Menyimpan formulir edit
    function updateresult(){
      var inc = $("#userid").val();
      var token = $('._token').data('token');
      var email = $('input[name=email]').val();
      var linked = $('input[name=linked]').val();
      var password_lama = $('input[name=password_lama]').val();
      var password_baru = $('input[name=password_baru]').val();
      var password_baru_retype = $('input[name=password_baru_retype]').val();
      var uri = linked+'/'+inc;

      if (password_lama=="")
      {
          $("#errorDivp").text("Password lama harus diisi.");
          $("#errorDiv").show();
          $("#password_lama").focus(); return 1;
      }else if (password_baru=="")
      {
          $("#errorDivp").text("Password baru harus diisi.");
          $("#errorDiv").show();
          $("#password_baru").focus(); return 1;
      }else if (password_baru_retype=="")
      {
          $("#errorDivp").text("Ulangi password baru harus diisi.");
          $("#errorDiv").show();
          $("#password_baru_retype").focus(); return 1;
      }else if (password_baru!=password_baru_retype)
      {
          $("#errorDivp").text("Ulangi Password baru tidak sama.");
          $("#errorDiv").show();
          $("#password_baru_retype").focus(); return 1;
      }
      else
      {
        $("#errorDiv").hide();
          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
            type: 'post',
            url: uri,
            data: {
                '_method':'PATCH',
                _token: token,
                'email': email,
                'password_lama': password_lama,
                'password_baru': password_baru
            },
            success: function(data) {
              $("#errorDiv").hide();
              if (data.error=="true") {
                $("#errorDivp").text(data.message);
                $("#errorDiv").show();
                $("#email").focus(); return 1;
              }else {
                window.location.href = data.url+"?message="+data.message;
              }

            },
          });
      }
    }
</script>
@endsection
