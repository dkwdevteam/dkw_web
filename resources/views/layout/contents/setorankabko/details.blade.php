@extends('layout.material-main')
@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Setoran Kabupaten</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active"><a href="{{url('setoran/kabupaten/history')}}">Setoran Kabupaten</a></li>
                            <li class="breadcrumb-item active">Detail Setoran</li>
                        </ol>
                    </div>
                </div>

                <button type="submit" class="btn btn-success" id="buttonPrint" style="margin-bottom: 15px;">Cetak Laporan</button>

                @if(session()->has('error'))
                <div class="alert alert-danger  alert-dismissible ">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
                  {{session()->get('error')}}
                </div>
                @endif

    <div class="card" id="printThis">
    <div class="card-block bg-info">
        <h3 class="text-white card-title">Rincian Setoran Kabupaten </h3>
    </div>
    <div class="card-block">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Nama Kota/Kabupaten</label>
                    <?php
                    $currUser = App\Models\Users::find(Sentinel::check()->id);
                    $kab = $currUser->getCities;
            				$prov = $kab->getProvince;
                    $dateObj   = DateTime::createFromFormat('!m', $setoran[0]->cash_month);
                    $monthName = $dateObj->format('F');
                    ?>
                    <input type="text" value="{{$kab->name}}, {{$prov->name}}" name="" class="form-control" readonly="">
                </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group">
                    <label>Status</label>
                    <input type="text" name="" value="{{$setoran[0]->status}}" class="form-control" readonly="">
                </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Bulan</label>
                <input type="text" name="" class="form-control" readonly="" value="{{$monthName}}">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Tahun</label>
                <input type="text" name="" class="form-control" readonly="" value="{{$setoran[0]->cash_year}}">
              </div>
            </div>
        </div>

        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12">
                <h4>Rincian</h4>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Kecamatan</th>
                            <th>Danabox</th>
                            <th>SP</th>
                            <th>Jumlah</th>
                            <th>Dana 15% Kecamatan</th>
                            <th>Total Setor</th>
                        </tr>
                        <?php
                        $in = 1;
                        $total_db=0;
                        $total_sp=0;
                        $total_jumlah=0;
                        $total_15persenkec=0;
                        $total_setorankekabupaten=0;
                        $total_15persenkab=0;
                        $total_15persenprov=0;
                        ?>
                        @foreach($setoran as $dk => $dv)
                        <tr>
                            <td>{{$in}}</td>
                            <td><a href="{{url('setoran/kabupaten/subdetails/'.$dv->id)}}">{{$dv->getKecamatan->name}}</a></td>
                            <td>Rp. {{number_format($dv->cash_db-$dv->db_10persen-$dv->db_15persen_1,0,',','.')}}</td>
                            <?php $total_db=$total_db+$dv->cash_db-$dv->db_10persen-$dv->db_15persen_1;  ?>
                            <td>Rp. {{number_format($dv->cash_sp-$dv->sp_15persen_1,0,',','.')}}</td>
                            <?php $total_sp=$total_sp+$dv->cash_sp-$dv->sp_15persen_1; ?>
                            <td>Rp. {{number_format($dv->cash_db+$dv->cash_sp-$dv->db_10persen-$dv->db_15persen_1-$dv->sp_15persen_1,0,',','.')}}</td>
                            <?php $total_jumlah=$total_jumlah+$dv->cash_db+$dv->cash_sp-$dv->db_10persen-$dv->db_15persen_1-$dv->sp_15persen_1; ?>
                            <td class="text-danger">-Rp. {{number_format($dv->db_15persen_2+$dv->sp_15persen_2,0,',','.')}}</td>
                            <?php
                            $total_15persenkec=$total_15persenkec+$dv->db_15persen_2+$dv->sp_15persen_2;
                            $total_15persenkab=$total_15persenkab+$dv->db_15persen_3+$dv->sp_15persen_3;
                            $total_15persenprov=$total_15persenprov+$dv->db_15persen_4+$dv->sp_15persen_4;
                            ?>
                            <td style="font-weight:bold;">Rp. {{number_format($dv->cash_db+$dv->cash_sp-$dv->db_10persen-$dv->db_15persen_1-$dv->db_15persen_2-$dv->sp_15persen_1-$dv->sp_15persen_2,0,',','.')}}</td>
                            <?php $total_setorankekabupaten=$total_setorankekabupaten+$dv->cash_db+$dv->cash_sp-$dv->db_10persen-$dv->db_15persen_1-$dv->db_15persen_2-$dv->sp_15persen_1-$dv->sp_15persen_2; ?>
                        </tr>
                        <?php $in ++;?>
                        @endforeach
                        <tr style="font-size: 19px;">
                            <td colspan="2"><b>Total</b></td>
                            <td><b>Rp. {{number_format($total_db,0,',','.')}} </b></td>
                            <td><b>Rp. {{number_format($total_sp,0,',','.')}} </b></td>
                            <td><b>Rp. {{number_format($total_jumlah,0,',','.')}}</b> </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr style="font-size: 19px;">
                            <td colspan="4">Total (Dana 15% Kecamatan se-Kabupaten)</td>
                            <td></td>
                            <td class="text-danger"><b>-Rp. {{number_format($total_15persenkec,0,',','.')}}</b> </td>
                            <td></td>
                        </tr>
                        <tr style="font-size: 20px;">
                            <td colspan="4">Total Setoran ke Kabupaten</td>
                            <td></td>
                            <td></td>
                            <td class=""><b>Rp. {{number_format($total_setorankekabupaten,0,',','.')}}</b> </td>
                        </tr>
                        <tr style="font-size: 20px;">
                          <td colspan="4">Dana 15% Kabupaten</td>
                          <td></td>
                          <td></td>
                          <td class="text-danger"><b>-Rp. {{number_format($total_15persenkab,0,',','.')}}</b> </td>
                        </tr>
                        <!-- <tr style="font-size: 20px;">
                          <td colspan="4">Dana 15% Provinsi</td>
                          <td></td>
                          <td></td>
                          <td class="text-danger"><b>-Rp. {{number_format($total_15persenprov,0,',','.')}}</b> </td>
                        </tr> -->
                        <tr style="font-size: 20px;">
                          <td colspan="4">Total Setoran ke Pusat</td>
                          <td></td>
                          <td></td>
                          <td class=""><b>Rp. {{number_format($total_setorankekabupaten-$total_15persenkab,0,',','.')}}</b> </td>
                        </tr>
                        <tr style="font-size: 20px;">
                          <td colspan="4">Total Transfer ke Pusat</td>
                          <td></td>
                          <td></td>
                          <td class=""><b>Rp. {{number_format($total_setorankekabupaten-$total_15persenkab,0,',','.')}}</b> </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-md-3">
                <label>Foto bukti setor</label>
                <a class="image-popup-no-margins" href="{{url('uploads/'.$setoran{0}->getBukti->getFoto->photo)}}" data-effect="mfp-zoom-in">
                  <img src="{{url('uploads/'.$setoran{0}->getBukti->getFoto->photo)}}" class="img-responsive">
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    </div>
@endsection
