@extends('layout.material-main')
@section('content')
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Sejarah Setoran</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{url('setoran/kabupaten/history')}}">Sejarah Setoran</a></li>
                            <li class="breadcrumb-item"><a href="{{ URL::previous() }}">Detail Setoran</a></li>
                            <li class="breadcrumb-item active">Sub Detail Setoran</li>
                        </ol>
                    </div>
                </div>
                @if(session()->has('error'))
                <div class="alert alert-danger  alert-dismissible ">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
                  {{session()->get('error')}}
                </div>
                @endif

                <button type="submit" class="btn btn-success" id="buttonPrint" style="margin-bottom: 15px;">Cetak Laporan</button>

    <div class="card" id="printThis">
    <div class="card-block bg-info">
        <h3 class="text-white card-title">Rincian Setoran Kecamatan </h3>
    </div>
    <div class="card-block">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Nama Kecamatan</label>
                    <?php $kecamatan = $setoran->getKecamatan;
                    $city = $kecamatan->getCities;
                    $prov = $city->getProvince;
                    ?>
                    <input type="text" value="{{$kecamatan->name}}, {{$city->name}}, {{$prov->name}}" name="" class="form-control" readonly="">
                </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group">
                    <label>Penghimpun</label>
                    <input type="text" name="" value="{{$setoran->people_deposits}}" class="form-control" readonly="">
                </div>
            </div>
            <div class="col-md-6">
                <label>Tanggal</label>
                <input type="text" name="" class="form-control" readonly="" value="{{date('d M Y', strtotime($setoran->cash_date))}}">
            </div>
        </div>

        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12">
                <h4>Rincian</h4>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Pengamal</th>
                            <th>Danabox</th>
                            <th>SP</th>
                            <th>Jumlah</th>
                            <th>Keterangan</th>
                        </tr>
                        <?php $details = $setoran->getDetails;
                        $in = 1;
                        ?>
                        @foreach($details as $dk => $dv)
                        <?php $people = $dv->getPeople;?>
                        <tr>
                            <td>{{$in}}</td>
                            <td>{{$people->nama}}<br><small>{{$people->alamat}}</small></td>
                            <td>Rp. {{number_format($dv->danabox,0,',','.')}}</td>
                            <td>Rp. {{number_format($dv->sp,0,',','.')}}</td>
                            <td>Rp. {{number_format($dv->sp+$dv->danabox,0,',','.')}}</td>
                            <td>{{$dv->detail}}</td>
                        </tr>
                        <?php $in ++;?>
                        @endforeach
                        <tr style="font-size: 19px;">
                            <td colspan="2"><b>Total</b></td>
                            <td><b>Rp. {{number_format($setoran->cash_db,0,',','.')}} </b></td>
                            <td><b>Rp. {{number_format($setoran->cash_sp,0,',','.')}} </b></td>
                            <td><b>Rp. {{number_format($setoran->cash_sp+$setoran->cash_db,0,',','.')}}</b> </td>
                            <td></td>
                        </tr>
                        <tr style="font-size: 19px;">
                            <td colspan="4">Dana 10% Penghimpun</td>
                            <td class="text-danger"><b>-Rp. {{number_format($setoran->db_10persen,0,',','.')}}</b> </td>
                            <td></td>
                        </tr>
                        <tr style="font-size: 19px;">
                            <td colspan="4">Dana 15% Jamaah</td>
                            <td class="text-danger"><b>-Rp. {{number_format($setoran->db_15persen_1+$setoran->sp_15persen_1,0,',','.')}}</b> </td>
                            <td></td>
                        </tr>
                        <tr style="font-size: 20px;">
                            <td colspan="4">Total Setoran ke Kecamatan</td>
                            <td class=""><b>Rp. {{number_format(($setoran->cash_sp+$setoran->cash_db)-$setoran->db_10persen-$setoran->db_15persen_1-$setoran->sp_15persen_1,0,',','.')}}</b> </td>
                            <td></td>
                        </tr>
                        <tr style="font-size: 19px;">
                          <td colspan="4">Dana 15% Kecamatan</td>
                          <td class=""><b>Rp. {{number_format($setoran->db_15persen_2+$setoran->sp_15persen_2,0,',','.')}}</b> </td>
                          <td></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <?php $user = Sentinel::check();?>
        @if($user->inRole('admin-kabko'))
        <div class="row" style="margin-top: 20px;">
            <div class="col-md-12">
                <form method="post" action="{{url('setoran/kabupaten/accept/'.$setoran->id)}}">
                {{csrf_field()}}
                <button class="btn btn-primary" type="submit" >Konfirmasi</button>
                </form>
            </div>
        </div>
        @endif
    </div>

    </div>
@endsection
