@extends('layout.material-main')
@section('content')
<div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Setoran Kabupaten</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Setoran Kabupaten</a></li>

                        </ol>
                    </div>
                </div>
                @if(session()->has('error'))
                <div class="alert alert-danger  alert-dismissible ">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
                  {{session()->get('error')}}
                </div>
                @endif

<div class="card">
    <div class="card-block bg-info">
        <h3 class="text-white card-title">Input Setoran Kabupaten </h3>
    </div>
    <div class="card-block">
    	<form method="post" action="{{url('setoran/kabupaten/store')}}" id="setor-form" enctype="multipart/form-data">
    		{{csrf_field()}}
    		<div class="form-group">
    			<div class="row">
    			<div class="col-12">
    				<?php $currUser = App\Models\Users::find(Sentinel::check()->id);?>
    				<label>Kabupaten</label>
    				<?php $kab = $currUser->getCities;
    				$prov = $kab->getProvince;
    				?>
    				<input type="text" name="" value="{{$kab->name}}" class="form-control" readonly="">
    				<input type="hidden" name="id_city" value="{{$kab->id}}">
    				<input type="hidden" name="id_region" value="{{$prov->id}}">
    				<input type="hidden" name="cash_date" value="{{date('Y-m-d')}}">

    			</div>
    			<div class="col-6 mt-10">

    				<label>Bulan</label>
    				<select class="form-control" name="bulan" readonly>
    					<?php
    						for($i = 1;$i<13;$i++):
    							$dateObj   = DateTime::createFromFormat('!m', $cash_month);
    							$monthName = $dateObj->format('F'); // March
    					?>
    					<option value="{{$cash_month}}" @if(date('m') == $i) selected @endif>{{$monthName}}</option>
    					<?php endfor;?>

    				</select>
    			</div>
    			<div class="col-6 mt-10">
    				<label>Tahun</label>
    				<select class="form-control" name="tahun" readonly>
    					<?php for($i = 2010; $i<2030; $i++):?>

    					<option value="{{$cash_year}}" @if(date('Y') == $i) selected="" @endif>{{$cash_year}}</option>
    					<?php endfor;?>

    				</select>
    			</div>
    			<div class="clearfix"></div>
    			</div>
    		</div>
        <div class="form-group">
    			<label>Data Setoran</label>
    			<div class="table-responsive" style="padding-top:10px">
                  <table class="display nowrap dataTable dtr-inline collapsed" id="postTable" >
                    <!-- style="visibility: hidden;" -->
                    <thead>
                        <tr>
                            <th width="55%">Kecamatan</th>
                            <th width="5%">Dana Box</th>
                            <th width="5%">SP</th>
                            <th width="5%">Jumlah</th>
                            <th width="5%">15% Kec</th>
                            <th width="5%">Total</th>
                            <th width="15%">Keterangan</th>
                        </tr>
                        {{ csrf_field() }}
                    </thead>
                    <tbody>
                    	<div id="setor-list">
                        <?php
                        $db_total = 0;
                        $sp_total = 0;
          							$total = 0;
              					$total15 = 0;
                        $total2 = 0;
                        $total15Kab = 0;
                        $totalTransfer = 0;

                        ?>
                        @foreach($approved_cash as $sk => $sv)
                        <tr>
                        <td>{{$sv->getKecamatan->name}}</td>
                        <td>Rp. {{number_format(($sv->cash_db-$sv->db_10persen-$sv->db_15persen_1),0,',','.')}}</td>
                        <?php
              							$db_total = $db_total+$sv->cash_db-$sv->db_10persen-$sv->db_15persen_1;
              					?>
                        <td>Rp. {{number_format(($sv->cash_sp-$sv->sp_15persen_1),0,',','.')}}</td>
                        <?php
              							$sp_total = $sp_total+$sv->cash_sp-$sv->sp_15persen_1;
              					?>
                        <td>Rp. {{number_format(($sv->cash_sp+$sv->cash_db-$sv->db_10persen-$sv->db_15persen_1-$sv->sp_15persen_1),0,',','.')}}</td>
                        <?php
                            $total = $total+$sv->cash_sp+$sv->cash_db-$sv->db_10persen-$sv->db_15persen_1-$sv->sp_15persen_1;
                        ?>
                        <td>Rp. {{number_format(($sv->db_15persen_2+$sv->sp_15persen_2),0,',','.')}}</td>
                        <?php
                            $total15 = $total15+$sv->db_15persen_2+$sv->sp_15persen_2;
                        ?>
                        <td>Rp. {{number_format(($sv->cash_sp+$sv->cash_db-$sv->db_10persen-$sv->db_15persen_1-$sv->db_15persen_2-$sv->sp_15persen_1-$sv->sp_15persen_2),0,',','.')}}</td>
                        <?php
                            $total2 = $total2+$sv->cash_sp+$sv->cash_db-$sv->db_10persen-$sv->db_15persen_1-$sv->db_15persen_2-$sv->sp_15persen_1-$sv->sp_15persen_2;
                            $total15Kab = $total15Kab+$sv->db_15persen_3+$sv->sp_15persen_3;
                            $totalTransfer = $totalTransfer+$sv->cash_sp+$sv->cash_db-$sv->db_10persen-$sv->db_15persen_1-$sv->db_15persen_2-$sv->db_15persen_3-$sv->sp_15persen_1-$sv->sp_15persen_2-$sv->sp_15persen_3;
                        ?>
                        <td>{{$sv->cash_descs}}</td>
                      </tr>
                        @endforeach
                    	</div>
                       	<tr>
                       		<td colspan="1" style="text-align: right;">Total</td>
                       		<td>
                       			<input type="text" name="db_total" readonly="" placeholder="Rp.0" value="{{number_format(($db_total),0,',','.')}}" id="db-total">
                       		</td>
                       		<td>
                       			<input type="text" name="sp_total" readonly="" placeholder="Rp.0" value="{{number_format(($sp_total),0,',','.')}}" id="sp-total">
                       		</td>
                       		<td>
                       			<input type="text" name="total" readonly="" placeholder="Rp.0" value="{{number_format(($total),0,',','.')}}" id="setor-total">
                       		</td>
                          <td>
                       			<input type="text" name="total15" readonly="" placeholder="Rp.0" value="{{number_format(($total15),0,',','.')}}" id="15-total">
                       		</td>
                          <td>
                       			<input type="text" name="total2" readonly="" placeholder="Rp.0" value="{{number_format(($total2),0,',','.')}}" id="total-total">
                       		</td>
                       	</tr>
                    </tbody>
                  </table>
                </div>
                <div class="row">
                  <div class="col-12">
                    <div class="row">
                      <div style="display: block; border-top: 1px solid #ddd; width: 100%; margin: 10px 0;"></div>

                          <div class="col-6" style="background:#e0ffdc;margin-left:5px;padding:5px">
                              Silahkan transfer ke rekening dibawah ini:
                              <br>
                              <p style="margin:5px;background:#c2f3bb;padding:5px;margin-right: 15px">
                                Rekening BNI <br/>Atas Nama <b style="font-size:20px">Abdul Latif Madjid</b> <br/>Nomor Rekening: <b style="font-size:20px">005 205 9786</b>
                              </p>
                              <p style="margin:7px;background:#c2f3bb;padding:5px;margin-right: 15px">
                                Rekening BRI <br/>Atas Nama <b style="font-size:20px">Abdul Latif Madjid</b> <br/>Nomor Rekening: <b style="font-size:20px">3204 01 000144 50 2</b>
                              </p>
                          </div>
                          <div class="col-6" style="background:#fbffc3;margin-left:-10px;padding:5px">
                            <table>
                              <tr style="font-size: 20px;">
                                <td style="width:70%">Total Setoran ke Kota/Kab.</td>
                                <td>Rp. </td>
                                <td class="pull-right" style="width:25%"><b class="pull-right">{{number_format($total2,0,',','.')}}</span></b> </td>
                              </tr>
                              <tr style="font-size: 20px;">
                                <td style="width:70%">Prosentase (15%) Kota/Kab.</td>
                                <td>Rp. </td>
                                <td class="text-danger pull-right" style="width:25%"><b class="pull-right">{{number_format($total15Kab,0,',','.')}}</span></b> </td>
                              </tr>
                              <tr style="font-size: 20px;">
                                <td style="width:70%">Total Setoran ke Pusat</td>
                                <td>Rp. </td>
                                <td class="pull-right" style="width:25%"><b class="pull-right">{{number_format($totalTransfer,0,',','.')}}</span></b> </td>
                              </tr>
                            </table>
                          </br>
                          <center>Total yang harus ditransfer ke Pusat senilai:</center>
                          </br>
                          <center><b style="font-size:40px">Rp. {{number_format($totalTransfer,0,',','.')}}</b></center>
                          </div>

                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <div class="row">
                    <div style="display: block; border-top: 1px solid #ddd; width: 100%; margin: 10px 0;"></div>
                    <div class="col-6">
                      <label>Upload Bukti</label>
                      <input type="file" name="" value="" id="img-select">
                      <input type="hidden" name="bukti" id="img-value">
                      <br>
                      <small>Sertakan bukti transfer berupa foto / scan</small>
                    </div>

                    <div class="col-3">
                      <img src="" class="img-responsive" id="img-preview">
                    </div>
                    </div>


                  </div>
                	<div class="col-12">
                		<div style="display: block; border-top: 1px solid #ddd; width: 100%; margin: 10px 0;"></div>
                		<label>Keterangan</label>
                		<textarea name="description" class="form-control" readonly>Total {{count($approved_cash)}} transaksi setoran kecamatan. @if(count($approved_cash)==0)Tidak ada data untuk disimpan.@endif</textarea>
                	</div>
                	<div class="col-12 mt-10">
                    @if(count($approved_cash)!=0)
                		<a href="javascript:void(0)" onclick="document.getElementById('setor-form').submit()" class="btn btn-primary pull-right">Simpan</a>
                    @endif
                    <a href="javascript:void(0)" onclick="history.back();" class="btn btn-warning pull-left">Kembali</a>
                	</div>
                </div>
    		</div>
    	</form>
    </div>
</div>


@endsection


@section('page-script')
<script type="text/javascript">
  function readFile() {

  if (this.files && this.files[0]) {

    var FR= new FileReader();

    FR.addEventListener("load", function(e) {
      document.getElementById("img-preview").src       = e.target.result;
      document.getElementById("img-value").value = e.target.result;
    });

    FR.readAsDataURL( this.files[0] );
  }

}

document.getElementById("img-select").addEventListener("change", readFile);
</script>
<script type="text/javascript">
  function checkForm(){
      var dfd = jQuery.Deferred();
      setTimeout(function checking(){

          if($("#img-value").val() === ""){
            alert("Mohon pilih bukti transfer");
            dfd.reject("Sorry");
          }
      }, 1000);

      setTimeout(function (){
        dfd.resolve("Completed");
      }, 2000);


      setTimeout(function working() {
          if ( dfd.state() === "pending" ) {
            dfd.notify( "working... " );
            setTimeout( working, 500 );
          }
        }, 1 );
      return dfd.promise();
    }
  $(document).ready(function(){
    $.fn.submitForm = function(){
      checkForm().done(function(){
        $("#form-book").submit();
      });
    };
  });
</script>
@endsection
