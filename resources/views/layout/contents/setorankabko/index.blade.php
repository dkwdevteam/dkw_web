@extends('layout.material-main')
@section('content')
<div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Setoran Kabupaten</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Setoran Kabupaten</a></li>

                        </ol>
                    </div>
                </div>
                @if(session()->has('error'))
                <div class="alert alert-danger  alert-dismissible ">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
                  {{session()->get('error')}}
                </div>
                @endif

<div class="card" id="printThis">
    <div class="card-block bg-info">
        <h3 class="text-white card-title">Input Setoran Kabupaten </h3>
    </div>
    <div class="card-block">
    	<form method="post" action="{{url('setoran/kabupaten/prestore')}}" id="setor-form">
    		{{csrf_field()}}
    		<div class="form-group">
    			<div class="row">
    			<div class="col-12">
    				<?php $currUser = App\Models\Users::find(Sentinel::check()->id);?>
    				<label>Kabupaten</label>
    				<?php $kab = $currUser->getCities;
    				$prov = $kab->getProvince;
    				?>
    				<input type="text" name="name_city" value="{{$kab->name}}" class="form-control" readonly="">
    				<input type="hidden" name="id_city" value="{{$kab->id}}">
    				<input type="hidden" name="id_region" value="{{$prov->id}}">
    				<input type="hidden" name="cash_date" value="{{date('Y-m-d')}}">

    			</div>
    			<div class="col-6 mt-10">

    				<label>Bulan</label>
    				<select class="form-control" name="bulan">
    					<?php
    						for($i = 1;$i<13;$i++):
    							$dateObj   = DateTime::createFromFormat('!m', $i);
    							$monthName = $dateObj->format('F'); // March
    					?>
    					<option value="{{$i}}" @if(date('m') == $i) selected @endif>{{$monthName}}</option>
    					<?php endfor;?>

    				</select>
    			</div>
    			<div class="col-6 mt-10">
    				<label>Tahun</label>
    				<select class="form-control" name="tahun">
    					<?php for($i = 2010; $i<2030; $i++):?>

    					<option value="{{$i}}" @if(date('Y') == $i) selected="" @endif>{{$i}}</option>
    					<?php endfor;?>

    				</select>
    			</div>
    			<div class="clearfix"></div>
    			</div>
          <div class="row">
            <div class="col-12 mt-10">
              <a href="javascript:void(0)" onclick="document.getElementById('setor-form').submit()" class="btn btn-primary pull-right">Siapkan Data</a>
            </div>
          </div>
    		</div>
    	</form>
    </div>
</div>


@endsection
