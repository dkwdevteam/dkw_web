@extends('layout.material-main')
@section('content')

                <div class="row">
                    <div class="col-lg-12 col-xlg-12 col-md-12">
                        <div class="card">
                            <!-- Tab Buttons -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#approval" role="tab">Notifikasi</a> </li>
                                {{-- <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#status" role="tab">Status Setoran</a> </li> --}}
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <!-- First Tab -->
                                <div class="tab-pane active" id="approval" role="tabpanel">
                                    <div class="card-block" style="overflow-y:scroll;">
                                        <div class="profiletimeline">
                                            {{-- {{print_r($notifikasi)}} --}}
                                            @if(count($notifikasi) > 0)
                                                @foreach($notifikasi as $nk => $nv)
                                                <div class="s1-item">
                                                    <div class="sl-left"><i class="mdi mdi-bell-outline"></i></div>
                                                    <div class="sl-right">
                                                        <div class="col-12 col-md-12">
                                                            <h3>{{$nv->name}}</h3>
                                                            <span class="sl-date">({{date('d M Y', strtotime($nv->date))}})
                                                            <span>| <a href="{{$nv->url}}" data-id-setoran="1">Lihat Detail</a></span>
                                                            
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                @endforeach
                                            @else
                                            <h3>Belum ada notifikasi</h3>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <!-- Second Tab -->
                                {{-- <div class="tab-pane" id="status" role="tabpanel">
                                    <div class="card-block" style="overflow-y:scroll;">
                                        <div class="profiletimeline">
                                            <div class="sl-item">
                                                <div class="sl-left"><i class="mdi mdi-bell-outline"></i></div>
                                                <div class="sl-right">
                                                    <div>
                                                        <a href="#" class="link">Setoran Juli 2018</a>
                                                        <span class="sl-date">(23 Juli 2018)</span>
                                                        <span>| <a href="#">Lihat Detail</a></span>
                                                        <span class="pull-right badge badge-default">Menunggu Persetujuan</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-item">
                                                <div class="sl-left"><i class="mdi mdi-bell-outline"></i></div>
                                                <div class="sl-right">
                                                    <div>
                                                        <a href="#" class="link">Setoran Juni 2018</a>
                                                        <span class="sl-date">(25 Juni 2018)</span>
                                                        <span>| <a href="#">Lihat Detail</a></span>
                                                        <span class="pull-right badge badge-success">Telah disetujui</span>
                                                        <span>| <a href="#">Ajukan Revisi</a></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-item">
                                                <div class="sl-left"><i class="mdi mdi-bell-outline"></i></div>
                                                <div class="sl-right">
                                                    <div>
                                                        <a href="#" class="link">Setoran Juni 2018</a>
                                                        <span class="sl-date">(25 Juni 2018)</span>
                                                        <span>| <a href="#">Lihat Detail</a></span>
                                                        <span class="pull-right badge badge-danger">Ditolak</span><br>
                                                        <span class="sl-date">Alasan Penolakan:</span>
                                                        <p><pre style="padding:2px 5px;">Data mohon disesuaikan</pre></p>
                                                        <span><a href="#" class="btn btn-primary btn-sm">Buat Revisi</a></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-item">
                                                <div class="sl-left"><i class="mdi mdi-bell-outline"></i></div>
                                                <div class="sl-right">
                                                    <div>
                                                        <a href="#" class="link">Setoran Mei 2018</a>
                                                        <span class="sl-date">(24 Mei 2018)</span>
                                                        <span>| <a href="#">Lihat Detail</a></span>
                                                        <span class="pull-right badge badge-success">Telah disetujui</span>
                                                        <span>| <a href="#">Ajukan Revisi</a></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="sl-item">
                                                <div class="sl-left"><i class="mdi mdi-bell-outline"></i></div>
                                                <div class="sl-right">
                                                    <div>
                                                        <a href="#" class="link">Setoran April 2018</a>
                                                        <span class="sl-date">(18 April 2018)</span>
                                                        <span>| <a href="#">Lihat Detail</a></span>
                                                        <span class="pull-right badge badge-success">Telah disetujui</span>
                                                        <span>| <a href="#">Ajukan Revisi</a></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    </div>
                                </div> --}}
                            </div>

                        </div>
                    </div>
                </div>

@endsection