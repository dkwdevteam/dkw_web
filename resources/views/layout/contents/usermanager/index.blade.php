@extends('layout.material-main')

@section('content')
<!-- <h1>Administrasi Pengguna</h1> -->
<div class="card">
  <div class="card-block bg-info">
      <h4 class="text-white card-title">Daftar Pengguna</h4>

         <!-- <form action="/manageuser/search" method="get" role="search" style="padding-bottom:15px">
           <div class="input-group custom-search-form">
             @if(isset($_GET['search']))
             <input type="text" class="form-control" name="search" id="search" value="{{$_GET['search']}}" placeholder="Cari">
             @else
             <input type="text" class="form-control" name="search" id="search" value="" placeholder="Cari">
             @endif

              {{csrf_field()}}
                <span class="input-group-btn">
                  <button class="btn btn-success" type="submit"><span class="mdi mdi-magnify"></span></button>
                </span>
           </div>
         </form> -->

  </div>
  <div class="card-block">
      <div class="message-box contact-box">
          <a href="{{url('manageuser/create')}}"><h2 class="add-ct-btn"><button type="button" class="btn btn-circle btn-lg btn-success waves-effect waves-dark"  data-toggle="tooltip" title="Tambah Pengguna">+</button></h2></a>
          <div class="message-widget contact-widget">

              @if(session()->has('message'))
              <div class="alert alert-success  alert-dismissible ">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="mdi mdi-information"></i><strong> Informasi!</strong><br>
                {{session()->get('message')}}
              </div>
              @endif
              @if(isset($_GET['message']))
              <div class="alert alert-success  alert-dismissible ">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="mdi mdi-information"></i><strong> Informasi!</strong><br>
                {{$_GET['message']}}
              </div>
              @endif


            <div class="table-responsive" style="padding-top:10px">
              <table class="display nowrap dataTable dtr-inline collapsed" id="postTable" >
                <!-- style="visibility: hidden;" -->
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Role User</th>
                        <th>Registered</th>
                        <th>Last Login</th>
                        <th>Actions</th>
                    </tr>
                    {{ csrf_field() }}
                </thead>
                <tbody>
                    @foreach($user as $index => $usr)
                    <!-- <tr class="item{{$usr->id}} @if($usr->id) warning @endif"> -->
                        <tr class="item{{$usr->user_id}}">
                            <td>{{$usr->username}}</td>
                            <td>{{$usr->email}}</td>
                            <td>{{$usr->slug}}</td>
                            <!-- <td class="text-center">
                              <label class="switch">
                                <input type="checkbox" checked>
                                <span class="slider sround"></span>
                              </label>
                            </td> -->
                            <td> @if ($usr->userscreated_at==null) Never @else {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $usr->userscreated_at)->diffForHumans()}} @endif</td>
                            <td> @if ($usr->last_login==null) Never @else {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $usr->last_login)->diffForHumans()}} @endif</td>
                            <!-- <td>
                                dd
                            </td> -->
                            <td>
                                <button class="show-modal btn btn-success" data-id="{{$usr->user_id}}" data-url="{{url('manageuser/show/'.$usr->user_id)}}">
                                <span class="mdi mdi-magnify"></span> Show</button>
                                <!-- <button class="edit-modal btn btn-info" data-id="{{$usr->user_id}}" data-url="{{url('manageuser/'.$usr->user_id)}}">
                                <span class="mdi mdi-pencil"></span> Edit</button> -->
                                <button class="delete-modal btn btn-danger" data-id="{{$usr->user_id}}" data-title="{{$usr->username}}" data-content="{{$usr->email}}">
                                <span class="mdi mdi-delete"></span> Delete</button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
            </div>



          </div>
      </div>
  </div>
</div>
@endsection

@section('page-script')
<script>
  $(function() {
    $("#postTable").dataTable();
  });

  $('.show-modal').on('click', function(){
    window.location.href = $(this).data('url');
  });

  $('.delete-modal').on('click', function(){
    alert("User `"+$(this).data('title')+"` apabila terdapat pada record transaksi setoran maka tidak dapat dihapus, apabila sebaliknya maka dapat dihapus.");
  });

</script>
@endsection
