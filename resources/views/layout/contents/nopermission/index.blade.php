@extends('layout.material-main')

@section('content')
<h1>Peringatan</h1>
<div class="card dkw-main-content bg-white mt-10">
  Anda tidak memiliki izin untuk dapat mengakses alamat/url: <br><br><strong>"{{$url}}"</strong>
</div>
@endsection
