@extends('layout.material-main')
@section('content')
<div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Sejarah Setoran</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Sejarah Setoran</a></li>

                        </ol>
                    </div>
                </div>
                @if(session()->has('error'))
                <div class="alert alert-danger  alert-dismissible ">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
                  {{session()->get('error')}}
                </div>
                @endif

<div class="card">
    <div class="card-block bg-info">
        <h3 class="text-white card-title">Daftar Setoran Provinsi </h3>
    </div>
    <div class="card-block">
      <div class="table-responsive" style="padding-top:10px">
        <table class="display nowrap dataTable dtr-inline collapsed" id="postTable">
          <thead>
            <tr>
              <th>No</th>
              <th>Bln</th>
              <th>Thn</th>
              <th>DB</th>
              <th>SP</th>
              <th>Total</th>
              <th>Status</th>
              <th width="20%">Pilihan</th>
            </tr>
          </thead>
          <tbody>
            <?php $in = 1;?>
            @foreach($setoran as $sk => $sv)
            <tr>
              <td>{{$in}}</td>
              <?php
              $in++;
              $dateObj   = DateTime::createFromFormat('!m', $sv->cash_month);
              $monthName = $dateObj->format('F');
              ?>
              <td>{{$monthName}}</td>
              <td>{{$sv->cash_year}}</td>
              <td><span class="">Rp.{{number_format(($sv->totalDB),0,',','.')}}</span></td>
              <td><span class="">Rp.{{number_format(($sv->totalSP),0,',','.')}}</span></td>
              <td><span class="">Rp.{{number_format(($sv->setoran),0,',','.')}}</span></td>
              <td>
                <?php
                $arr = explode("-",$sv->status, 2);
                ?>
                @switch($arr[0])
                @case("ditolak")
                <span class="mdi mdi-led-on text-danger"></span>
                @break
                @case("diajukan")
                <span class="mdi mdi-led-on text-warning"></span>
                @break
                @default
                <span class="mdi mdi-led-on text-success"></span>
                @endswitch
                {{$arr[1]}}
              </td>
              <td>
                <a href="{{url('setoran/provinsi/details/'.$sv->cash_month.'/'.$sv->cash_year.'/'.$sv->status)}}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> Lihat Detail</a>
                <a href="#" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i> Request Edit</a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
</div>


@endsection

@section('page-script')
<script>
  $(function() {
    $("#postTable").dataTable();
  });
</script>
@endsection
