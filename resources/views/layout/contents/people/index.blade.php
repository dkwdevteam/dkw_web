@extends('layout.material-main')
@section('content')


<div class="card dkw-main-content bg-white mt-10">
<h2 class="dkw-main-title">Data Jamaah</h2>
	<div class="dkw-people-form">
		<div class="form-group">
			<label>Pilih Provinsi</label>
			<select class="form-control select2" id="selprovince" style="width: 100%;">
				<option value="-1">Pilih Provinsi</option>
				@foreach($provinces as $pk => $pv)
				<option value="{{$pv->id}}">{{$pv->name}}</option>
				@endforeach
			</select>
		</div>

		<div class="form-group" id="selcity-wrap">
			<label>Pilih Kabupaten / kota</label>
			<select class="form-control select2" id="selcity" style="width: 100%;">

			</select>
		</div>

		<div class="form-group" id="selkecamatan-wrap">
			<label>Pilih Kecamatan</label>

			<select class="form-control select2" id="selkecamatan" style="width: 100%;">
			</select>
		</div>

		<div class="form-group" id="seldesa-wrap">
			<label>Pilih Desa</label>

			<select class="form-control select2" id="seldesa" style="width: 100%;">
				
			</select>
		</div>
	</div>
</div>



<div class="card-block dkw-main-content bg-white mt-10" id="jamaah-table-wrap">

	<h3>List Data Jamaah</h3>
	<div class="message-box contact-box">
	<a href="javascript:void(0);" data-toggle="modal" data-target="#exampleModal"><h2 class="add-ct-btn"><button type="button" class="btn btn-circle btn-lg btn-success waves-effect waves-dark btn-add-jamaah"  data-toggle="tooltip" title="Tambah Jamaah">+</button></h2></a>
	</div>
	<span id="table-spinner" class="text-center fa fa-spinner fa-spin fa-5x" style="width: 100%;"></span>
	<div class="table-responsive m-t-20">
    <table class="table stylish-table" id="jamaah-table">
		<thead>
			<tr>
				<th>No</th>
				<th>Nama</th>
				<th>Alamat</th>
				<th>No. Telp</th>
				<th>Grup</th>
				<th></th>
			</tr>
		</thead>
		<tbody id="people-list-wrap">
		</tbody>
	</table>
	</div>
</div>



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Jamaah</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="add-form-wrap">
        	<div class="form-group">
        		<label>Nama</label>
        		<input type="text" id="add-nama" name="" class="form-control" placeholder="Masukan nama">
        		<span id="add-nama-err-msg" style="color: red; font-size: 10px;">Harap masukan nama</span>
        	</div>
        	<div class="form-group">
        		<label>Alamat</label>
        		<textarea class="form-control" id="add-alamat" placeholder="Masukan alamat"></textarea>
        		<span id="add-alamat-err-msg" style="color: red; font-size: 10px;">Harap masukan alamat</span>
        	</div>
        	<div class="form-group">
        		<label>No. Telp</label>
        		<input type="text" name="" id="add-phone" class="form-control" placeholder="Masukan no. telp.">
        	</div>
        	<div class="form-group">
        		<label>Group</label>
        		<select class="form-control" id="add-group">
        			<option value="bapak">Bapak</option>
        			<option value="ibu">Ibu-ibu</option>
        			<option value="remaja">Remaja</option>
        			<option value="kanak">Anak-anak</option>
        		</select>
        	</div>

        	<div class="form-group" id="add-is-anggota-wrap">
        		<label>Anggota Keluarga</label>
        		<select class="form-control" id="add-is-anggota">
        			<option value="1">Tidak</option>
        			<option value="0">Ya</option>
        		</select>
        	</div>
        	<div class="form-group" id="add-sel-kk-wrap">
        		<label>Kepala Keluarga</label>
        		<select class="form-control select2"  id="add-sel-kk" style="width: 100%;">
        			<option value="-">---</option>
        		</select>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="button" id="btn-add-people" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Data Jamaah</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="add-form-wrap">
        	<div class="form-group">
        		<label>Nama</label>
        		<input type="text" name="" id="edit-nama" class="form-control" placeholder="Masukan nama">
        		<span id="edit-nama-err-msg" style="color: red; font-size: 10px;">Harap masukan nama</span>
        	</div>
        	<div class="form-group">
        		<label>Alamat</label>
        		<textarea class="form-control" id="edit-alamat" placeholder="Masukan alamat"></textarea>
        		<span id="edit-alamat-err-msg" style="color: red; font-size: 10px;">Harap masukan alamat</span>
        	</div>
        	<div class="form-group">
        		<label>No. Telp</label>
        		<input type="text" name="" id="edit-phone" class="form-control" placeholder="Masukan no. telp.">
        	</div>
        	<div class="form-group">
        		<label>Group</label>
        		<select class="form-control" id="edit-group">
        			<option value="bapak">Bapak</option>
        			<option value="ibu">Ibu-ibu</option>
        			<option value="remaja">Remaja</option>
        			<option value="kanak">Anak-anak</option>
        		</select>
        	</div>

        	<div class="form-group" id="edit-is-anggota-wrap">
        		<label>Anggota Keluarga</label>
        		<select class="form-control" id="edit-is-anggota">
        			<option value="1">Tidak</option>
        			<option value="0">Ya</option>
        		</select>
        	</div>
        	<div class="form-group" id="edit-sel-kk-wrap">
        		<label>Kepala Keluarga</label>
        		<select class="form-control select2" id="edit-sel-kk" style="width: 100%;">
        			<option value="-">---</option>
        		</select>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="button" id="edit-btn-save" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-danger" >
        <h5 class="modal-title text-white " id="exampleModalLabel">Hapus Data Jamaah</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="">
        	Apakah anda yakin ingin menghapus data ?

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-danger btn-delete">Hapus</button>
      </div>
    </div>
  </div>
</div>


@endsection

@section('page-script')

<script type="text/x-tmpl" id="tmpl-list">
<tr>
	<td>{%=o.number%}</td>
	<td>{%=o.nama%}</td>
	<td>{%=o.alamat%}</td>
	<td>{%=o.telp%}</td>
	<td>{%=o.group%}</td>
	<td>
		<a href="#" data-toggle="modal" class="btn-edit" data-target="#editModal" data-id="{%=o.id%}" style="display: inline-block!important;"><button class="btn btn-warning waves-effect waves-light" type="button"><span class="btn-label"><i class="fa fa-pencil"></i></span>Ubah</button></a>
                  <a data-toggle="modal" data-id="{%=o.id%}" class="btn-deleting" data-target="#deleteModal" style="display: inline-block!important;"><button class="btn btn-danger waves-effect waves-light del-list" type="button"  id="del-link" data-roleid="#" data-rownum="#"><span class="btn-label"><i class="fa fa-times"></i></span>Hapus</button></a>                              <a style="display: inline-block!important;"></a>
	</td>
</tr>
</script>
<script type="text/x-tmpl" id="tmpl-list-empty">
<tr>
	<td>Belum ada data</td>
</tr>
</script>

<script type="text/javascript">
	$(document).ready(function(){

		var editid = -1;
		var deleteid = -1;

		$("#selcity-wrap").hide();
		$("#selkecamatan-wrap").hide();

		$("#seldesa-wrap").hide();
		$("#jamaah-table").hide();
		$("#jamaah-table-wrap").hide();

		$(".select2").select2();

		$.fn.provinceSelected = function(province){
			var url = "{{url('api/v1/getcity')}}";
			$("#selcity").empty();
			$.post(url, {province:province})
				.done(function(result){
					if(result.status == false){
						alert(result.message);
						$("#selcity-wrap").hide();

					} else{
						console.log(result.data);
						$("#selcity-wrap").show(500);
						$.each(result.data, function(key, value){
							var appstr = '<option value="' + value.id + '">' + value.name + '</option>';
							$("#selcity").append(appstr);
						});
					}
				});
		}

		$.fn.citySelected = function(city){
			var url = "{{url('api/v1/getdistrict')}}";
			$("#selkecamatan").empty();
			$.post(url, {city:city})
				.done(function(result){
					if(result.status == false){
						alert(result.message);
						$("#selkecamatan-wrap").hide();
					} else {
						console.log(result.data);
						$("#selkecamatan-wrap").show(500);
						$.each(result.data, function(key,value){
							var appstr = '<option value="' + value.id + '">' + value.name + '</option>';
							$("#selkecamatan").append(appstr);
						});
						
					}
				});

		}


		$.fn.loadDataJamaah = function(city, kecamatan){
			var url = "{{url('api/v1/loadpeople')}}";
			$("#people-list-wrap").empty();
			$.post(url, {city:city,kecamatan:kecamatan})
				.done(function(result){


					if(result.total_data > 0){
						var number = 1;
						$.each(result.data, function(index,value){
							value.number = number;
							$("#people-list-wrap").append(tmpl("tmpl-list", value));
							number ++;
						});	
					} else {
						$("#people-list-wrap").append(tmpl("tmpl-list-empty"));
					}
					$("#table-spinner").hide();
					$("#jamaah-table").show(1000);
					
				});
		}
		// $.fn.provinceSelected($("#selprovince").val());

		$(document).on('change', '#selprovince', function(){
			$.fn.provinceSelected($(this).val());
		});

		$(document).on('change', '#selcity', function(){
			$.fn.citySelected($(this).val());
		});

		$(document).on('click', '.btn-add-jamaah', function(){
			$("#add-sel-kk-wrap").hide();
		});


		$("#add-nama-err-msg").hide();
		$("#add-alamat-err-msg").hide();

		$.fn.insertPeople = function(city,kecamatan){

			var url = "{{url('api/v1/insertpeople')}}";

			$("#add-nama-err-msg").hide();
			$("#add-alamat-err-msg").hide();
			var name = $("#add-nama").val();
			if(name === ""){
				$("#add-nama").focus();
				$("#add-nama-err-msg").show();
				return 0;
			}
			var alamat = $("#add-alamat").val();
			if(alamat === ""){
				$("#add-alamat").focus();
				$("#add-alamat-err-msg").show();
				return 0;
			}

			var phone = $("#add-phone").val();
			var group = $("#add-group").val();
			var isanggota = $("#add-is-anggota").val();
			var kk = $("#add-sel-kk").val();

			$.post(url, {kecamatan:kecamatan,city:city,nama:name,alamat:alamat,telp:phone,group:group,is_kk:isanggota,kk_id:kk})
				.done(function(result){
					if(result.status === true){
						$("#exampleModal").modal('toggle');
						$.fn.loadDataJamaah(city,kecamatan);
					} else {

					}
				});
		}

		$.fn.editPeople = function(id){
			var city = $("#selcity").val();
			var kecamatan = $("#selkecamatan").select2('val');
			var url = "{{url('api/v1/editpeople')}}";
			$("#edit-nama-err-msg").hide();
			$("#edit-alamat-err-msg").hide();

			var name = $("#edit-nama").val();
			if(name === ""){
				$("#edit-nama").focus();
				$("#edit-nama-err-msg").show();
				return 0;
			}
			var alamat = $("#edit-alamat").val();
			if(alamat === ""){
				$("#edit-alamat").focus();
				$("#edit-alamat-err-msg").show();
				return 0;
			}

			var phone = $("#edit-phone").val();
			var group = $("#edit-group").val();
			var isanggota = $("#edit-is-anggota").val();
			var kk = $("#edit-sel-kk").val();

			$.post(url, {id:id,kecamatan:kecamatan,city:city,nama:name,alamat:alamat,telp:phone,group:group,is_kk:isanggota,kk_id:kk})
				.done(function(result){
					if(result.status === true){
						$("#editModal").modal('toggle');
						$.fn.loadDataJamaah(city,kecamatan);
					} else {

					}
				});

		}


		$("#edit-btn-save").click(function(){
			$.fn.editPeople(editid);
		});

		$("#btn-add-people").click(function(){
			var city = $("#selcity").val();
			var kecamatan  = $("#selkecamatan").val();
			
			$.fn.insertPeople(city,kecamatan);
		});


		$.fn.getListKK = function (add,kk_id){
			var url = "{{url('api/v1/getlistkk')}}";
			var city = $("#selcity").val();
			var kecamatan  = $("#selkecamatan").val();

			$.post(url, {city:city,kecamatan:kecamatan})
				.done(function(result){
					if(add === 1){
						$("#add-sel-kk").empty();
						$.each(result.data, function(index,value){
							$("#add-sel-kk").append('<option value="' +value.id + '">' + value.nama + '</option>');
						});
					} else {
						$("#edit-sel-kk").empty();
						$.each(result.data, function(index,value){
							$("#edit-sel-kk").append('<option value="' +value.id + '">' + value.nama + '</option>');
						});
						$("#edit-sel-kk").val(kk_id);
					}
				});
		}

		$(document).on('change', '#add-is-anggota', function(e){
			var status = $(this).val();
			if(status === "0"){
				$("#add-sel-kk-wrap").show();
				$.fn.getListKK(1);
			} else {
				$("#add-sel-kk-wrap").hide();
			}
		});

		$(document).on('change', '#edit-is-anggota', function(e){
			var status = $(this).val();
			if(status === "0"){
				$("#edit-sel-kk-wrap").show();
				$.fn.getListKK(0);
			} else {
				$("#edit-sel-kk-wrap").hide();
			}
		});

		$(document).on('click', ".btn-edit", function(e){
			var url = "{{url('api/v1/getdatapeople')}}";
			var id = $(this).data('id');
			$("#edit-nama-err-msg").hide();
			$("#edit-alamat-err-msg").hide();
			$.post(url, {id:id})
				.done(function(result){
					if(result.status === true){
						var data = result.data;
						editid = data.id;
						$("#edit-nama").val(data.nama);
						$("#edit-alamat").val(data.alamat);
						$("#edit-phone").val(data.telp);
						$("#edit-group").val(data.group);
						if(data.is_kk == 0){
							$("#edit-is-anggota").val(0);
							$("#edit-sel-kk-wrap").show();
							$.fn.getListKK(0,data.kk_id);
							$("#edit-sel-kk").val(data.kk_id);
							
						} else {
							$("#edit-is-anggota").val(1);
							$("#edit-sel-kk-wrap").hide();
						}

					}
				});
		});

		$(document).on('click', ".btn-deleting", function(e){
			deleteid = $(this).data('id');
		});

		$(".btn-delete").click(function(){
			var city = $("#selcity").val();
			var kecamatan  = $("#selkecamatan").val();
			if(deleteid > 0){
				var url = "{{url('api/v1/deletepeople')}}";
				$.post(url, {id:deleteid})
					.done(function(result){
						if(result.status === true){
							$("#deleteModal").modal('toggle');
							$.fn.loadDataJamaah(city,kecamatan);
						}
					});
			}
		});

		$(document).on('change', '#selkecamatan', function(){
			$("#table-spinner").show();
			$("#people-list-wrap").empty();
			$("#jamaah-table-wrap").show(1000);
			var city = $("#selcity").val();
			var kecamatan = $("#selkecamatan").select2('val');
			$.fn.loadDataJamaah(city,kecamatan);
		});
	});

</script>
@endsection