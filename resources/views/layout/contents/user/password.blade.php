@extends('layout.material-main')

@section('content')
<div class="card">
  <div class="card-block bg-info">
      <h4 class="text-white card-title">Ubah Pengguna</h4>
  </div>
  <div class="card-block">
      <div class="message-box contact-box">
          <!-- <div class="message-widget contact-widget"> -->
            @if(session()->has('message'))
            <div class="alert alert-warning  alert-dismissible ">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
              {{session()->get('message')}}
            </div>
            @endif
            <div class="alert alert-warning  alert-dismissible " id="errorDiv" style="display:none;">
              <!-- <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> -->
              <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
              <p id="errorDivp"></p>
            </div>

            <form class="form-horizontal form-material">
              <!-- <input type="hidden" name="_method" value="post"> -->
                  <div class="form-group">
                      <label for="example-email" class="col-md-12">Email</label>
                      <div class="col-md-12">
                          <input type="email" placeholder="..." class="form-control form-control-line" name="email" id="email" value="{{$currentchosenuser[0]->email}}" required email>
                      </div>
                  </div>
                  <!-- <div class="form-group">
                      <label class="col-md-12">Password Lama</label>
                      <div class="col-md-12">
                          <input type="password" placeholder="..." value="" class="form-control form-control-line" id="password_lama" name="password_lama">
                      </div>
                  </div> -->
                  <div class="form-group">
                      <label class="col-md-12">Password Baru</label>
                      <div class="col-md-12">
                          <input type="password" placeholder="..." value="" class="form-control form-control-line" id="password_baru" name="password_baru">
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-md-12">Ulangi Password Baru</label>
                      <div class="col-md-12">
                          <input type="password" placeholder="..." value="" class="form-control form-control-line" id="password_retype" name="password_retype">
                      </div>
                  </div>
                <!-- <div class="form-group">
                    <label class="col-md-12">Register and activated ?</label>
                    <div class="col-md-12">
                      <input type="radio" name="registerandactivated" value="Yes" checked="true"> Yes<br>
                      <input type="radio" name="registerandactivated" value="No"> No<br>
                    </div>
                </div> -->
                <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
                <input type="hidden" id="userid" name="userid" value="{{$currentchosenuser[0]->user_id}}">
                <input type="hidden" id="linked" name="linked" value="{{url('/usersadministration/epass/')}}">
                <div class="form-group">
                    <div class="col-sm-12">
                        <a class="btn btn-success  mdi mdi-content-save btnUpdateClick"style="color: white;">Update</a>
                        <a href="{{URL::previous()}} " class="btn btn-warning mdi mdi-arrow-left-bold">Kembali</a>
                    </div>
                </div>
            </form>

          <!-- </div> -->
      </div>
  </div>
</div>
@endsection

@section('page-script')
<script>
  $(function() {
    $("#password_baru").focus(); return 1;
  });


  $('.btnUpdateClick').on('click', function(){
      var inc = $("#userid").val();
      var token = $('._token').data('token');
      var email = $('input[name=email]').val();
      var linked = $('input[name=linked]').val();
      // var password_lama = $('input[name=password_lama]').val();
      var password_baru = $('input[name=password_baru]').val();
      var password_retype = $('input[name=password_retype]').val();
      var uri = linked+'/'+inc;

      // if (password_lama=="")
      // {
      //     $("#errorDivp").text("Password lama harus diisi.");
      //     $("#errorDiv").show();
      //     $("#password_lama").focus();
      //     window.scrollTo(0, 0); return 1;
      // }else
      if (password_baru=="")
      {
          $("#errorDivp").text("Password baru harus diisi.");
          $("#errorDiv").show();
          $("#password_baru").focus();
          window.scrollTo(0, 0); return 1;
      }else if (password_retype=="")
      {
          $("#errorDivp").text("Ulangi password baru harus diisi.");
          $("#errorDiv").show();
          $("#password_retype").focus();
          window.scrollTo(0, 0); return 1;
      }else if (password_baru!=password_retype)
      {
          $("#errorDivp").text("Ulangi Password baru tidak sama.");
          $("#errorDiv").show();
          $("#password_baru_retype").focus(); return 1;
      }
      else
      {
        $("#errorDiv").hide();
          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
            type: 'POST',
            url: uri,
            data: {
                '_method':'PATCH',
                _token: token,
                'email': email,
                'password_baru': password_baru
            },
            success: function(data) {
              $("#errorDiv").hide();
              if (data.error=="true") {
                $("#errorDivp").text(data.message);
                $("#errorDiv").show();
                $("#email").focus(); return 1;
              }else {
                window.location.href = data.url+"?message="+data.message;
              }
            },
          });
      }
  });

</script>
@endsection
