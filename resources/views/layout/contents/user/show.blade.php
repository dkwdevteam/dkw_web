@extends('layout.material-main')

@section('content')
<div class="card">
  <div class="card-block bg-info">
      <h4 class="text-white card-title">Detail Pengguna</h4>
  </div>
  <div class="card-block">
      <div class="message-box contact-box">
          <div class="message-widget contact-widget">
            <div class="row">
              <div class="col-lg-4 col-xlg-3 col-md-5">
                @if(session()->has('message'))
                <div class="alert alert-success  alert-dismissible ">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <i class="mdi mdi-information"></i><strong> Informasi!</strong><br>
                  {{session()->get('message')}}
                </div>
                @endif
                @if(isset($_GET['message']))
                <div class="alert alert-success  alert-dismissible ">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <i class="mdi mdi-information"></i><strong> Informasi!</strong><br>
                  {{$_GET['message']}}
                </div>
                @endif

                <div class="card-body">
                  <small class="text-muted">User Role</small>
                    <div style="padding-bottom: 10px;">
                      <button class="btn btn-circle btn-secondary"><i class="mdi mdi-account-star-variant"></i></button>
                      <?php $this_province=App\Models\Provinces::where('id',$currentchosenuser[0]->province_id)->first() ?>
                      <span style="font-size: smaller;">I'm a {{$currentchosenuser[0]->name}} | {{$this_province->name}}</span>
                    </div>
                  <div class="text-muted p-t-30 db">
                    <hr>
                  </div>
                  <small class="text-muted">User info</small>
                  <br>
                  <br>
                  <div style="padding-bottom: 10px;">
                    <button class="btn btn-circle btn-secondary"><i class="mdi mdi-account-key"></i></button>
                    <span style="font-size: smaller;">Last login at @if ($currentchosenuser[0]->last_login==null) Never @else {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $currentchosenuser[0]->last_login)->diffForHumans()}} @endif</span>
                  </div>
                  <div style="padding-bottom: 10px;">
                    <button class="btn btn-circle btn-secondary"><i class="mdi mdi-auto-fix"></i></button>
                    <span style="font-size: smaller;">Created at @if ($currentchosenuser[0]->userscreated_at==null) Never @else {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $currentchosenuser[0]->userscreated_at)->diffForHumans()}} @endif</span>
                  </div>
                  <div style="padding-bottom: 10px;">
                    <button class="btn btn-circle btn-secondary"><i class="mdi mdi-account-convert"></i></button>
                    <span style="font-size: smaller;">Updated at @if ($currentchosenuser[0]->usersupdated_at==null) Never @else {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $currentchosenuser[0]->usersupdated_at)->diffForHumans()}} @endif</span>
                  </div>
                </div>
              </div>
              <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card-body">
                  <small class="text-muted">First Name</small>
                    <h6>{{$currentchosenuser[0]->first_name}}</h6>
                  <small class="text-muted p-t-30 db">Last Name</small>
                    <h6>{{$currentchosenuser[0]->last_name}}</h6>
                  <small class="text-muted p-t-30 db">Email</small>
                    <h6>{{$currentchosenuser[0]->email}}</h6>
                  <small class="text-muted p-t-30 db">Username</small>
                    <h6>{{$currentchosenuser[0]->username}}</h6>
                  <small class="text-muted p-t-30 db">Phone Number</small>
                    <h6>{{$currentchosenuser[0]->phone_number}}</h6>
                  <small class="text-muted p-t-30 db">Mobile Number</small>
                    <h6>{{$currentchosenuser[0]->mobile_number}}</h6>
                  <br>
                </div>
              </div>
            </div>



            <span class="pull-left">
              <button class="back-modal btn btn-warning" data-url="{{url('usersadministration')}}">
              <span class="mdi mdi-arrow-left-bold"></span> Kembali</button>
              <!-- <a href="{{url()->previous()}}" data-original-title="Kembali" data-toggle="tooltip" data-animation="false"type="button" class="btn btn-sm btn-warning"><i class="mdi mdi-arrow-left-bold"></i> Kembali</a> -->
            </span>
            <span class="pull-right">
              <!-- <button class="edit-modal btn btn-info" data-id="{{$currentchosenuser[0]->user_id}}" data-url="{{url('usersadministration/'.$currentchosenuser[0]->user_id)}}">
              <span class="mdi mdi-pencil"></span> Ubah Profil</button> -->
              <button class="editpass-modal btn btn-info" data-id="{{$currentchosenuser[0]->user_id}}" data-url="{{url('usersadministration/pass/'.$currentchosenuser[0]->user_id)}}">
              <span class="mdi mdi-lock"></span> Ubah Password</button>
            </span>
          </div>
      </div>
  </div>
</div>
@endsection

@section('page-script')
<script>
  $(function() {

  });

  $('.edit-modal').on('click', function(){
    window.location.href = $(this).data('url');
  });

  $('.editpass-modal').on('click', function(){
    window.location.href = $(this).data('url');
  });

  $('.back-modal').on('click', function() {
    window.location.href = $(this).data('url');
  });
</script>
@endsection
