@extends('layout.material-main')

@section('content')
<div class="card">
  <div class="card-block bg-info">
      <h4 class="text-white card-title">Buat Pengguna</h4>
  </div>
  <div class="card-block">
      <div class="message-box contact-box">
          <!-- <div class="message-widget contact-widget"> -->
            @if(session()->has('message'))
            <div class="alert alert-warning  alert-dismissible ">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
              {{session()->get('message')}}
            </div>
            @endif
            <div class="alert alert-warning  alert-dismissible " id="errorDiv" style="display:none;">
              <!-- <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> -->
              <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
              <p id="errorDivp"></p>
            </div>

            <form class="form-horizontal form-material">
              <input type="hidden" name="_method" value="POST">
              {{csrf_field()}}
                <div class="form-group">
                    <label class="col-md-12">First Name</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="..." class="form-control form-control-line" value="" id="first_name" name="first_name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Last Name</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="..." class="form-control form-control-line" value="" id="last_name" name="last_name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Username</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="..." class="form-control form-control-line" value="" id="username" name="username" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="example-email" class="col-md-12">Email</label>
                    <div class="col-md-12">
                        <input type="email" placeholder="..." class="form-control form-control-line" name="email" id="email" value="" required email>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Password</label>
                    <div class="col-md-12">
                        <input type="password" placeholder="..." value="" class="form-control form-control-line" id="password" name="password">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Ulangi Password</label>
                    <div class="col-md-12">
                        <input type="password" placeholder="..." value="" class="form-control form-control-line" id="password_retype" name="password_retype">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Phone No</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="..." class="form-control form-control-line" value="" id="phone_number" name="phone_number">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Mobile No</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="..." class="form-control form-control-line" value="" id="mobile_number" name="mobile_number">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-12">Role</label>
                    <div class="col-sm-12">
                        <select class="form-control form-control-line" id="role" name="role">
                            <option value="x">-- Choose a role --</option>
                            @if(Sentinel::getUser()->inRole('super-admin'))
                              @foreach($roles as $index => $d)
                                @if(($d->slug=="super-admin") || ($d->slug=="admin-pusat"))
                                  <option value="{{$d->id}}">{{$d->name}}</option>
                                @endif
                              @endforeach
                            @endif
                            @if(Sentinel::getUser()->inRole('admin-pusat'))
                              @foreach($roles as $index => $d)
                                @if(($d->slug=="admin-provinsi") || ($d->slug=="admin-pusat"))
                                  <option value="{{$d->id}}" data-slug="{{$d->slug}}">{{$d->name}}</option>
                                @endif
                              @endforeach
                            @endif
                            @if(Sentinel::getUser()->inRole('admin-provinsi'))
                              @foreach($roles as $index => $d)
                                @if(($d->slug=="admin-kabko"))
                                  <option value="{{$d->id}}" data-slug="{{$d->slug}}">{{$d->name}}</option>
                                @endif
                              @endforeach
                            @endif
                            @if(Sentinel::getUser()->inRole('admin-kabko'))
                              @foreach($roles as $index => $d)
                                @if(($d->slug=="admin-kecamatan"))
                                  <option value="{{$d->id}}" data-slug="{{$d->slug}}">{{$d->name}}</option>
                                @endif
                              @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="form-group" id="selprovince-wrap">
            			<label class="col-md-12">Pilih Provinsi</label>
                  <div class="col-sm-12">
            			<select class="form-control select2" id="selprovince" style="width: 100%;">
            				<option value="-1">Pilih Provinsi</option>
            				@foreach($provinces as $pk => $pv)
            				<option value="{{$pv->id}}">{{$pv->name}}</option>
            				@endforeach
            			</select>
                </div>
            		</div>

            		<div class="form-group" id="selcity-wrap">
            			<label class="col-md-12">Pilih Kabupaten / kota</label>
                  <div class="col-sm-12">
            			<select class="form-control select2" id="selcity" style="width: 100%;">
                    <option value="-1">Pilih Kabupaten / kota</option>
            				@foreach($cities as $pk => $pv)
            				<option value="{{$pv->id}}">{{$pv->name}}</option>
            				@endforeach
            			</select>
                </div>
            		</div>

            		<div class="form-group" id="selkecamatan-wrap">
            			<label class="col-md-12">Pilih Kecamatan</label>
                  <div class="col-sm-12">
                    <select class="form-control select2" id="selkecamatan" style="width: 100%;">
                    <option value="-1">Pilih Kecamatan</option>
            				@foreach($districts as $pk => $pv)
            				<option value="{{$pv->id}}">{{$pv->name}}</option>
            				@endforeach
            			</select>
                </div>
            		</div>
                <!-- <div class="form-group">
                    <label class="col-md-12">Register and activated ?</label>
                    <div class="col-md-12">
                      <input type="radio" name="registerandactivated" value="Yes" checked="true"> Yes<br>
                      <input type="radio" name="registerandactivated" value="No"> No<br>
                    </div>
                </div> -->
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="hidden" name="uri" value="{{url('usersadministration')}}">
                        <a class="btn btn-success  mdi mdi-content-save" onclick="save()" style="color: white;">Simpan</a>
                        <a href="{{URL::previous()}} " class="btn btn-warning mdi mdi-arrow-left-bold">Kembali</a>
                    </div>
                </div>
            </form>

          <!-- </div> -->
      </div>
  </div>
</div>
@endsection

@section('page-script')
<script>
$(document).ready(function(){
    $("#selprovince-wrap").hide();
    $("#selcity-wrap").hide();
		$("#selkecamatan-wrap").hide();
    $(".select2").select2();
    $("#first_name").focus(); return 1;

  });

  $(document).on('change', '#role', function(){
    if ($(this).find(':selected').data('slug')=="admin-provinsi") {
      $("#selprovince-wrap").show();
    }else if ($(this).find(':selected').data('slug')=="admin-kabko") {
      $("#selcity-wrap").show();
    }else if ($(this).find(':selected').data('slug')=="admin-kecamatan") {
      $("#selkecamatan-wrap").show();
    }else{
      $("#selprovince-wrap").hide();
    }
  });

  function save(){

    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var token = $('._token').data('token');
    var first_name = $('input[name=first_name]').val();
    var last_name = $('input[name=last_name]').val();
    var username = $('input[name=username]').val();
    var email = $('input[name=email]').val();
    var password = $('input[name=password]').val();
    var password_retype = $('input[name=password_retype]').val();
    var phone_number = $('input[name=phone_number]').val();
    var mobile_number = $('input[name=mobile_number]').val();
    var role = $('#role option:selected').val();
    var roleslug = $('#role option:selected').data('slug');
    var provinsi = $('#selprovince option:selected').val();
    var kabko = $('#selcity option:selected').val();
    var kecamatan = $('#selkecamatan option:selected').val();
    var uri = $('input[name=uri]').val();
    $("#errorDiv").hide();

    if (first_name=="")
    {
      $("#errorDivp").text("First name harus diisi.");
      $("#errorDiv").show();
      $("#first_name").focus();
      window.scrollTo(0, 0); return 1;
    }else if (last_name=="")
    {
      $("#errorDivp").text("Last name harus diisi.");
      $("#errorDiv").show();
      $("#last_name").focus();
      window.scrollTo(0, 0); return 1;
    }else if (username=="")
    {
        $("#errorDivp").text("Username harus diisi.");
        $("#errorDiv").show();
        $("#username").focus();
        window.scrollTo(0, 0); return 1;
    }else if (!email.match(mailformat))
    {
        $("#errorDivp").text("Email tidak valid.");
        $("#errorDiv").show();
        $("#email").focus();
        window.scrollTo(0, 0); return 1;
    }else if (password=="")
    {
        $("#errorDivp").text("Password harus diisi.");
        $("#errorDiv").show();
        $("#password").focus();
        window.scrollTo(0, 0); return 1;
    }else if (password_retype=="")
    {
        $("#errorDivp").text("Ulangi password harus diisi.");
        $("#errorDiv").show();
        $("#password_retype").focus();
        window.scrollTo(0, 0); return 1;
    }else if (password!=password_retype)
    {
        $("#errorDivp").text("Ulangi Password baru tidak sama.");
        $("#errorDiv").show();
        $("#password_baru_retype").focus();
        window.scrollTo(0, 0); return 1;
    }else if (role=="x")
    {
        $("#errorDivp").text("Pilih role dahulu.");
        $("#errorDiv").show();
        $("#role").focus();
        window.scrollTo(0, 0); return 1;
    }else if ((roleslug=="admin-provinsi")&&(provinsi=="-1"))
    {
        $("#errorDivp").text("Pilih provinsi dahulu.");
        $("#errorDiv").show();
        $("#selprovince").focus();
        window.scrollTo(0, 0); return 1;
    }
    else
    {
      $("#errorDiv").hide();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          type: 'post',
          url: uri,
          data: {
              '_method':'POST',
              _token: token,
              'first_name': first_name,
              'last_name': last_name,
              'username': username,
              'email': email,
              'phone_number': phone_number,
              'mobile_number': mobile_number,
              'password': password,
              'role': role,
              'provinsi': provinsi,
              'kabko': kabko,
              'kecamatan': kecamatan
          },
          success: function(data) {
            $("#errorDiv").hide();
            if (data.error=="false") {
              window.location.href = data.url+"?message="+data.message;
            }
          },
        });
    }
  }
</script>
@endsection
