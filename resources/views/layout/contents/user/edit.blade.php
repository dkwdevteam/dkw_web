@extends('layout.material-main')

@section('content')
<div class="card">
  <div class="card-block bg-info">
      <h4 class="text-white card-title">Ubah Pengguna</h4>
  </div>
  <div class="card-block">
      <div class="message-box contact-box">
          <!-- <div class="message-widget contact-widget"> -->
            @if(session()->has('message'))
            <div class="alert alert-warning  alert-dismissible ">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
              {{session()->get('message')}}
            </div>
            @endif
            <div class="alert alert-warning  alert-dismissible " id="errorDiv" style="display:none;">
              <!-- <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> -->
              <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
              <p id="errorDivp"></p>
            </div>

            <form class="form-horizontal form-material">
                <div class="form-group">
                    <label class="col-md-12">First Name</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="..." class="form-control form-control-line" value="{{$currentchosenuser[0]->first_name}}" id="first_name" name="first_name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Last Name</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="..." class="form-control form-control-line" value="{{$currentchosenuser[0]->last_name}}" id="last_name" name="last_name" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Username</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="..." class="form-control form-control-line" value="{{$currentchosenuser[0]->username}}" id="username" name="username" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="example-email" class="col-md-12">Email</label>
                    <div class="col-md-12">
                        <input type="email" placeholder="..." class="form-control form-control-line" name="email" id="email" value="{{$currentchosenuser[0]->email}}" required email>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Phone No</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="..." class="form-control form-control-line" value="{{$currentchosenuser[0]->phone_number}}" id="phone_number" name="phone_number">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Mobile No</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="..." class="form-control form-control-line" value="{{$currentchosenuser[0]->mobile_number}}" id="mobile_number" name="mobile_number">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-12">Role</label>
                    <div class="col-sm-12">
                        <select class="form-control form-control-line" id="role" name="role">
                            <option value="x">-- Choose a role --</option>
                            @foreach($roles as $index => $d)
                            <option value="{{$d->id}}" @if ($d->id==$currentchosenuser[0]->role_id) selected @endif>{{$d->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <!-- <div class="form-group">
                    <label class="col-md-12">Register and activated ?</label>
                    <div class="col-md-12">
                      <input type="radio" name="registerandactivated" value="Yes" checked="true"> Yes<br>
                      <input type="radio" name="registerandactivated" value="No"> No<br>
                    </div>
                </div> -->
                <input type="hidden" name="_token" id="csrf-token" value="{{ csrf_token() }}" />
                <input type="hidden" id="userid" name="userid" value="{{$currentchosenuser[0]->user_id}}">
                <div class="form-group">
                    <div class="col-sm-12">
                        <a class="btn btn-success  mdi mdi-content-save btnUpdateClick" style="color: white;">Update</a>
                        <a href="{{URL::previous()}} " class="btn btn-warning mdi mdi-arrow-left-bold">Kembali</a>
                    </div>
                </div>
            </form>

          <!-- </div> -->
      </div>
  </div>
</div>
@endsection

@section('page-script')
<script>
  $(function() {
    $("#first_name").focus(); return 1;
  });

  $('.btnUpdateClick').on('click', function(){
      var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      var inc = $("#userid").val();
      var token = $('._token').data('token');
      var first_name = $('input[name=first_name]').val();
      var last_name = $('input[name=last_name]').val();
      var username = $('input[name=username]').val();
      var email = $('input[name=email]').val();
      var password = $('input[name=password]').val();
      var password_retype = $('input[name=password_retype]').val();
      var phone_number = $('input[name=phone_number]').val();
      var mobile_number = $('input[name=mobile_number]').val();
      var role = $('#role option:selected').val();
      var uri = '/usersadministration/'+inc;

      $("#errorDiv").hide();

      if (first_name=="")
      {
        $("#errorDivp").text("First name harus diisi.");
        $("#errorDiv").show();
        $("#first_name").focus();
        window.scrollTo(0, 0); return 1;
      }else if (last_name=="")
      {
        $("#errorDivp").text("Last name harus diisi.");
        $("#errorDiv").show();
        $("#last_name").focus();
        window.scrollTo(0, 0); return 1;
      }else if (username=="")
      {
          $("#errorDivp").text("Username harus diisi.");
          $("#errorDiv").show();
          $("#username").focus();
          window.scrollTo(0, 0); return 1;
      }else if (!email.match(mailformat))
      {
          $("#errorDivp").text("Email tidak valid.");
          $("#errorDiv").show();
          $("#email").focus();
          window.scrollTo(0, 0); return 1;
      }else if (role=="x")
      {
          $("#errorDivp").text("Pilih role dahulu.");
          $("#errorDiv").show();
          $("#role").focus();
          window.scrollTo(0, 0); return 1;
      }
      else
      {
        $("#errorDiv").hide();
          $.ajaxSetup({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
          });
          $.ajax({
            type: 'POST',
            url: uri,
            data: {
                '_method':'PATCH',
                _token: token,
                'first_name': first_name,
                'last_name': last_name,
                'username': username,
                'email': email,
                'phone_number': phone_number,
                'mobile_number': mobile_number,
                'role': role
            },
            success: function(data) {
              $("#errorDiv").hide();
              if (data.error=="true") {
                $("#errorDivp").text(data.message);
                $("#errorDiv").show();
                $("#email").focus(); return 1;
              }else {
                window.location.href = data.url+"?message="+data.message;
              }
            },
          });
      }
  });
</script>
@endsection
