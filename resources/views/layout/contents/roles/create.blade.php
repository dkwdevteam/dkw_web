@extends('layout.material-main')

@section('content')
<div class="card">
  <div class="card-block bg-info">
      <h4 class="text-white card-title">Buat Role</h4>
  </div>
  <div class="card-block">
      <div class="message-box contact-box">
          <!-- <div class="message-widget contact-widget"> -->
            @if(session()->has('message'))
            <div class="alert alert-warning  alert-dismissible ">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
              {{session()->get('message')}}
            </div>
            @endif
            <div class="alert alert-warning  alert-dismissible " id="errorDiv" style="display:none;">
              <!-- <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> -->
              <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
              <p id="errorDivp"></p>
            </div>

            <form class="form-horizontal form-material">
              <input type="hidden" name="_method" value="POST">
              {{csrf_field()}}
                <div class="form-group">
                    <label class="col-md-12">Slug</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="..." class="form-control form-control-line" value="" id="slug" name="slug" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Name</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="..." class="form-control form-control-line" value="" id="name" name="name" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <a class="btn btn-success  mdi mdi-content-save" onclick="save()" style="color: white;">Simpan</a>
                        <a href="{{URL::previous()}} " class="btn btn-warning mdi mdi-arrow-left-bold">Kembali</a>
                    </div>
                </div>
            </form>

          <!-- </div> -->
      </div>
  </div>
</div>
@endsection

@section('page-script')
<script>
  $(function() {
    $("#slug").focus(); return 1;
  });

  function save(){
    var token = $('._token').data('token');
    var slug = $('input[name=slug]').val();
    var name = $('input[name=name]').val();
    var uri = '/roles';
    $("#errorDiv").hide();

    if (slug=="")
    {
      $("#errorDivp").text("Slug harus diisi.");
      $("#errorDiv").show();
      $("#slug").focus();
      window.scrollTo(0, 0); return 1;
    }else if (name=="")
    {
      $("#errorDivp").text("Name harus diisi.");
      $("#errorDiv").show();
      $("#name").focus();
      window.scrollTo(0, 0); return 1;
    }
    else
    {
      $("#errorDiv").hide();
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          type: 'post',
          url: uri,
          data: {
              '_method':'POST',
              _token: token,
              'slug': slug,
              'name': name
          },
          success: function(data) {
            $("#errorDiv").hide();
            if (data.error=="false") {
              window.location.href = data.url+"?message="+data.message;
            }
          },
        });
    }
  }
</script>
@endsection
