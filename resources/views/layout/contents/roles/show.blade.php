@extends('layout.material-main')

@section('content')
<div class="card">
  <div class="card-block bg-info">
      <h4 class="text-white card-title">Ubah Role</h4>
  </div>
  <div class="card-block">
      <!-- <div class="message-box contact-box">
          <div class="message-widget contact-widget"> -->

            @if(session()->has('message'))
            <div class="alert alert-warning  alert-dismissible ">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
              {{session()->get('message')}}
            </div>
            @endif
            <div class="alert alert-warning  alert-dismissible " id="errorDiv" style="display:none;">
              <!-- <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> -->
              <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
              <p id="errorDivp"></p>
            </div>

            <form class="form-horizontal form-material">
              <input type="hidden" name="_method" value="PATCH">
              {{csrf_field()}}
                <div class="form-group">
                    <label class="col-md-12">Slug</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="..." class="form-control form-control-line" id="slug" name="slug" value="{{$roleLists->slug}}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Name</label>
                    <div class="col-md-12">
                        <input type="text" placeholder="..." class="form-control form-control-line" id="name" name="name" value="{{$roleLists->name}}" required>
                    </div>
                </div>
                <input type="hidden" id="roleid" name="roleid" value="{{$roleLists->id}}">
                <div class="form-group">
                    <div class="col-sm-12">
                        <a class="btn btn-success  mdi mdi-content-save" onclick="update()" style="color: white;">Update</a>
                        <a href="{{URL::previous()}} " class="btn btn-warning mdi mdi-arrow-left-bold">Kembali</a>
                    </div>
                </div>
            </form>

          <!-- </div>
      </div> -->
  </div>
</div>
@endsection

@section('page-script')
<script>
  $(function() {

  });

  //Menyimpan formulir edit
  function update(){
    var inc = $("#roleid").val();
    var token = $('._token').data('token');
    var name = $('input[name=name]').val();
    var slug = $('input[name=slug]').val();
    var uri = '/roles/'+inc;

    if (slug=="")
    {
        $("#errorDivp").text("Slug harus diisi.");
        $("#errorDiv").show();
        $("#slug").focus(); return 1;
    }else if (name=="")
    {
        $("#errorDivp").text("Name harus diisi.");
        $("#errorDiv").show();
        $("#name").focus(); return 1;
    }
    else
    {
      $("#errorDiv").hide();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
      $.ajax({
        type: 'POST',
        url: uri,
        data: {
            '_method':'patch',
            _token: token,
            'slug': slug,
            'name': name
        },
        success: function(data) {
          $("#errorDiv").hide();
          if (data.error=="true") {
            $("#errorDivp").text(data.message);
            $("#errorDiv").show();
            $("#email").focus(); return 1;
          }else {
            window.location.href = data.url+"?message="+data.message;
          }

        },
      });
    }
  }
</script>
@endsection
