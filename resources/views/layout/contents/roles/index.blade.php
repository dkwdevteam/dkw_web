@extends('layout.material-main')

@section('content')
<!-- <h1>Administrasi Pengguna</h1> -->
<div class="card">
  <div class="card-block bg-info">
      <h4 class="text-white card-title">Daftar Roles</h4>

         <form action="/roles/search" method="get" role="search" style="padding-bottom:15px">
           <div class="input-group custom-search-form">
              @if(isset($_GET['search']))
              <input type="text" class="form-control" name="search" id="search" value="{{$_GET['search']}}" placeholder="Cari">
              @else
              <input type="text" class="form-control" name="search" id="search" value="" placeholder="Cari">
              @endif
              {{csrf_field()}}
                <span class="input-group-btn">
                  <button class="btn btn-success" type="submit"><span class="mdi mdi-magnify"></span></button>
                </span>
           </div>
         </form>

  </div>
  <div class="card-block">
      <div class="message-box contact-box">
          <a href="{{url('roles/create')}}"><h2 class="add-ct-btn"><button type="button" class="btn btn-circle btn-lg btn-success waves-effect waves-dark"  data-toggle="tooltip" title="Tambah Role">+</button></h2></a>
          <div class="message-widget contact-widget">

              @if(session()->has('message'))
              <div class="alert alert-success  alert-dismissible ">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="mdi mdi-information"></i><strong> Informasi!</strong><br>
                {{session()->get('message')}}
              </div>
              @endif
              @if(isset($_GET['message']))
              <div class="alert alert-success  alert-dismissible ">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <i class="mdi mdi-information"></i><strong> Informasi!</strong><br>
                {{$_GET['message']}}
              </div>
              @endif
              <div class="alert alert-warning  alert-dismissible " id="errorDiv" style="display:none;">
                <!-- <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> -->
                <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
                <p id="errorDivp"></p>
              </div>
              <div class="alert alert-success  alert-dismissible " id="successDiv" style="display:none;">
                <!-- <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> -->
                <i class="mdi mdi-information"></i><strong> Informasi!</strong><br>
                <p id="successDivp"></p>
              </div>

              <!-- Message -->
              <div class="table-responsive m-t-20">
                <table class="table stylish-table" id="datalist">
                    <thead>
                        <tr>
                            <th colspan="2">Roles</th>
                            <th>Name</th>
                            <!-- <th>Priority</th> -->
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach($roleLists as $index => $usr)
                        <tr id="row{{$index+1}}" class="row-list">
                            <td style="width:50px;"><span class="round {{$rounds[array_rand($rounds,1)]}}">{{substr($usr->name,0,1)}}</span></td>
                            <td>
                                <h6>{{$usr->name}}</h6><small class="text-muted">{{$usr->slug}}</small></td>
                            <td>{{$usr->name}}</td>
                            <!-- <td><span class="label label-success">Low</span></td> -->
                            <td>
                              <a href="{{url('roles/'.$usr->id)}}" style="display: inline-block!important;"><button class="btn btn-warning waves-effect waves-light" type="button"><span class="btn-label"><i class="fa fa-pencil"></i></span>Ubah</button></a>
                              <a style="display: inline-block!important;"><button class="btn btn-danger waves-effect waves-light del-list" type="button"  id="del-link" data-roleid="{{$usr->id}}" data-rownum="{{$index+1}}"><span class="btn-label"><i class="fa fa-times"></i></span>Hapus</button></a>                              <a style="display: inline-block!important;"></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
              </div>

              <div style="padding-top:15px">
                {{$roleLists->links()}}
              </div>
          </div>
      </div>
  </div>
</div>
@endsection

@section('page-script')
<script>
  $(function() {

  });

  $('#datalist').on('click', '#del-link', function(){
    var rownum =$(this).closest('tr').attr('id');
    var inc =$(this).data('roleid');
    console.log(rownum);
    console.log(inc);
    var token = $('._token').data('token');
    var uri = '/roles/'+inc;

    $("#errorDiv").hide();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $.ajax({
      type: 'POST',
      url: uri,
      data: {
          '_method':'delete',
          _token: token,
          'id': inc
      },
      success: function(data) {
        $(".alert").hide();
        if (data.error=="true"){
            $("#errorDivp").text(data.message);
            $("#errorDiv").show();
        }else{
            $("#successDivp").text(data.message);
            $("#successDiv").show();
            // var row = document.getElementById('row'+rownum);
            $('#'+rownum).remove();
        }
      }
    });

  });
</script>
@endsection
