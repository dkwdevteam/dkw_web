@extends('layout.material-main')
@section('content')
<div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Bukti Setoran</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Bukti Setoran</a></li>

                        </ol>
                    </div>
                </div>
                @if(session()->has('error'))
                <div class="alert alert-danger  alert-dismissible ">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <i class="mdi mdi-alert"></i><strong> Peringatan!</strong><br>
                  {{session()->get('error')}}
                </div>
                @endif

<div class="card">
    <div class="card-block bg-info">
        <h3 class="text-white card-title">Bukti Setoran </h3>
    </div>
    <div class="card-block">
      <div class="table-responsive" style="padding-top:10px">
        <table class="display nowrap dataTable dtr-inline collapsed" id="postTable">
          <thead>
            <tr>
              <th>No</th>
              <th>Kota Pengirim</th>
              <th>Bln</th>
              <th>Thn</th>
              <th>DB</th>
              <th>SP</th>
              <th>Total</th>
              <th>P</th>
              <th>Status</th>
              <th width="20%">Pilihan</th>
            </tr>
          </thead>
          <?php $in = 1;?>
          <tbody>
            @foreach($buktisetor as $sk => $sv)
            <tr>
              <td>{{$in}}</td>
              <?php
              $in++;
              $dateObj   = DateTime::createFromFormat('!m', $sv->cash_month);
              $monthName = $dateObj->format('F');
              $this_province_id = App\Models\Cities::where('id',$sv->id_city)->first();
              $this_province = App\Models\Provinces::where('id',$this_province_id->province_id)->first();
              ?>
              <td>{{ucwords(strtolower($sv->getKabko->name))}},</br>{{ucwords(strtolower($this_province->name))}}</td>
              <td>{{$monthName}}</td>
              <td>{{$sv->cash_year}}</td>
              <td>
                <?php
                $temp=0;?>
                @foreach($sv->getDetails as $dk => $dv)
                <?php
                $temp=$temp
                +$dv->getDetails->cash_db
                -$dv->getDetails->db_10persen
                -$dv->getDetails->db_15persen_1
                -$dv->getDetails->db_15persen_2
                -$dv->getDetails->db_15persen_3;
                ?>
                @endforeach
                Rp.{{number_format(($temp),0,',','.')}}
              </td>
              <td>
                <?php
                $temp=0;?>
                @foreach($sv->getDetails as $dk => $dv)
                <?php
                $temp=$temp
                +$dv->getDetails->cash_sp
                -$dv->getDetails->sp_15persen_1
                -$dv->getDetails->sp_15persen_2
                -$dv->getDetails->sp_15persen_3;
                ?>
                @endforeach
                Rp.{{number_format(($temp),0,',','.')}}
              </td>
              <td>
                <?php
                $temp=0;?>
                @foreach($sv->getDetails as $dk => $dv)
                <?php
                $temp=$temp
                +$dv->getDetails->cash_db
                +$dv->getDetails->cash_sp
                -$dv->getDetails->db_10persen
                -$dv->getDetails->db_15persen_1
                -$dv->getDetails->db_15persen_2
                -$dv->getDetails->db_15persen_3
                -$dv->getDetails->sp_15persen_1
                -$dv->getDetails->sp_15persen_2
                -$dv->getDetails->sp_15persen_3;
                ?>
                @endforeach
                <b>Rp.{{number_format(($temp),0,',','.')}}</b>
              </td>
              <td>
                <div id="image-popups" class="row">
                  <div class="col-lg-12 col-md-1 col-sm-2">
                    <a class="image-popup-no-margins" href="{{url('uploads/'.$sv->photo)}}" data-effect="mfp-zoom-in">
                      <img src="{{url('uploads/'.$sv->photo)}}" class="center-cropped" width="40" height="40" style="border-radius:50%">
                    </a>
                  </div>
                </div>
              </td>
              <td>
                @foreach($sv->getDetails as $dk => $dv)
                @if(count($sv->getDetails)!=1)
                <?php
                  $arr = explode("-",$dv->getDetails->status, 2);
                 ?>
                   @switch($arr[0])
                       @case("ditolak")
                            <span class="mdi mdi-led-on text-danger"></span>
                           @break
                       @case("diajukan")
                            <span class="mdi mdi-led-on text-warning"></span>
                           @break
                       @default
                           <span class="mdi mdi-led-on text-success"></span>
                   @endswitch
                  {{$arr[1]}}
                @break
                @else
                <?php
                  $arr = explode("-",$dv->getDetails->status, 2);
                 ?>
                   @switch($arr[0])
                       @case("ditolak")
                            <span class="mdi mdi-led-on text-danger"></span>
                           @break
                       @case("diajukan")
                            <span class="mdi mdi-led-on text-warning"></span>
                           @break
                       @default
                           <span class="mdi mdi-led-on text-success"></span>
                   @endswitch
                  {{$arr[1]}}
                @break
                @endif
                @endforeach
              </td>
              <td>
                <a href="{{url('buktisetoran/pusat/details/'.$sv->id)}}" class="btn btn-sm btn-primary" style="margin-bottom: 10px;"><i class="fa fa-eye"></i> Lihat Detail</a>
              </td>
            </tr>
          @endforeach
        </tbody>
        </table>
      </div>
    </div>
</div>
@endsection

@section('page-script')
<script>
  $(function() {
    $("#postTable").dataTable();
  });
</script>
@endsection
