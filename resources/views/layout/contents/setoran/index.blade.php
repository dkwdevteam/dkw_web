@extends('layout.material-main')
@section('content')

                <div class="row">
                    <!-- column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-block">
                                <form class="form-inline"   >
                                    <div class="form-group col-3 col-md-3">
                                        <label>Kecamatan</label>
                                        <select id="opt_kecamatan" name="opt_kecamatan" class="form-control form-control-sm">
                                            <option value="1">Kecamatan A</option>
                                            <option value="2">Kecamatan B</option>
                                            <option value="3">Kecamatan C</option>
                                            <option value="4">Kecamatan D</option>
                                            <option value="5">Kecamatan E</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-3 col-md-3">
                                        <label>Bulan</label>
                                        <select id="opt_bulan" name="opt_bulan" class="form-control form-control-sm">
                                            <option value="1">Jan</option>
                                            <option value="2">Feb</option>
                                            <option value="3">Mar</option>
                                            <option value="4" selected="selected">April</option>
                                            <option value="5">Dst...</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-3 col-md-3">
                                        <label>Tahun</label>
                                        <select id="opt_tahun" name="opt_tahun" class="form-control form-control-sm">
                                            <option value="2017">2017</option>
                                            <option value="2018" selected="selected">2018</option>
                                            <option value="2019">2019</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-3 col-md-3">
                                        <input type="button" id="btn_filter" name="btn_filter" value="Cari" class="btn btn-primary">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-block">
                                <h4 class="card-title">Setoran Juni 2018</h4>
                                <h6 class="card-subtitle">25 Juni 2018</h6>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="text-center">No</th>
                                                <th class="text-center">Nama</th>
                                                <th class="text-center">DB</th>
                                                <th class="text-center">SP</th>
                                                <th class="text-center">Infaq</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-center">1</td>
                                                <td class="text-center">Pengamal A</td>
                                                <td class="text-right">57,700</td>
                                                <td class="text-right">50,000</td>
                                                <td class="text-right">-</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">2</td>
                                                <td class="text-center">Pengamal B</td>
                                                <td class="text-right">57,700</td>
                                                <td class="text-right">50,000</td>
                                                <td class="text-right">-</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">3</td>
                                                <td class="text-center">Pengamal C</td>
                                                <td class="text-right">57,700</td>
                                                <td class="text-right">-</td>
                                                <td class="text-right">-</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">4</td>
                                                <td class="text-center">Pengamal D</td>
                                                <td class="text-right">57,700</td>
                                                <td class="text-right">50,000</td>
                                                <td class="text-right">-</td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">5</td>
                                                <td class="text-center">Pengamal E</td>
                                                <td class="text-right">57,700</td>
                                                <td class="text-right">50,000</td>
                                                <td class="text-right">-</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="2" class="text-right"><strong>Jumlah</strong></td>
                                                <td class="text-right"><strong>350,000</strong></td>
                                                <td class="text-right"><strong>560,200</strong></td>
                                                <td class="text-right"><strong>25,000</strong></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="text-right"><strong>Petugas 10%</strong></td>
                                                <td class="text-right">35,000</td>
                                                <td class="text-right"></td>
                                                <td class="text-right"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="text-right"><strong>Jamaah 15%</strong></td>
                                                <td class="text-right">46,700</td>
                                                <td class="text-right">54,200</td>
                                                <td class="text-right"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="text-right"><strong>Kecamatan 15%</strong></td>
                                                <td class="text-right">37,400</td>
                                                <td class="text-right">48,800</td>
                                                <td class="text-right"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="text-right"><strong>Setoran ke Kab/Ko</strong></td>
                                                <td class="text-right"><strong>350,200</strong></td>
                                                <td class="text-right"><strong>336,600</strong></td>
                                                <td class="text-right"><strong>25,000</strong></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

@endsection