@extends('layout.material-main')

@section('content')
<div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Setoran Kecamatan</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Setoran Kecamatan</a></li>

                        </ol>
                    </div>
                </div>

<div class="card">
<div class="card-block bg-info">
	<h3 class="text-white card-title">Input Setoran Kecamatan</h3>
</div>
<div class="card-block">
	<form method="post" action="{{url('setoran/kecamatan/store')}}" id="setor-form">
		{{csrf_field()}}
		<div class="form-group">
			<div class="row">
			<div class="col-12">
				<?php $currUser = App\Models\Users::find(Sentinel::check()->id);?>
				<label>Kecamatan</label>
				<?php $kec = $currUser->getKecamatan;
				$kab = $kec->getCities;
				$prov = $kab->getProvince;
				$pengamal = App\Models\People::where('kec_id', $kec->id)->where('is_kk',1)->get();
				?>
				<input type="text" name="" value="{{$kec->name}}" class="form-control" readonly="">
				<input type="hidden" name="id_kecamatan" value="{{$kec->id}}">
				<input type="hidden" name="id_city" value="{{$kab->id}}">
				<input type="hidden" name="id_region" value="{{$prov->id}}">
				<input type="hidden" name="cash_date" value="{{date('Y-m-d')}}">

			</div>
			<div class="col-6 mt-10">

				<label>Bulan</label>
				<select class="form-control" name="bulan">
					<?php
						for($i = 1;$i<13;$i++):
							$dateObj   = DateTime::createFromFormat('!m', $i);
							$monthName = $dateObj->format('F'); // March
					?>
					<option value="{{$i}}" @if(date('m') == $i) selected @endif>{{$monthName}}</option>
					<?php endfor;?>

				</select>
			</div>
			<div class="col-6 mt-10">
				<label>Tahun</label>
				<select class="form-control" name="tahun">
					<?php for($i = 2010; $i<2030; $i++):?>

					<option value="{{$i}}" @if(date('Y') == $i) selected="" @endif>{{$i}}</option>
					<?php endfor;?>

				</select>
			</div>
			<div class="col-12">
				<br>
				<label>Penghimpun</label>
				<input type="text" name="penghimpun" class="form-control" placeholder="Masukan nama Penghimpun" required="">
			</div>
			<div class="clearfix"></div>
			</div>
		</div>
		<div class="form-group">
			<label>Data Setoran</label>
			<div class="table-responsive" style="padding-top:10px">
              <table class="display nowrap dataTable dtr-inline collapsed" id="postTable" >
                <!-- style="visibility: hidden;" -->
                <thead>
                    <tr>
                        <th width="25%">Pengamal</th>
                        <th width="10%">Dana Box</th>
                        <th width="10%">SP</th>
                        <th width="10%">Jumlah</th>
                        <th width="15%">Keterangan</th>
                    </tr>
                    {{ csrf_field() }}
                </thead>
                <tbody>
                	<div id="setor-list">

                	</div>
                   	<tr id="last-sort">
                   		<td colspan="4"><a href="javascript:void(0)"  id="add-row" class="btn btn-info btn-sm">Tambah </a> <span> tersedia (<span id="pengamal-count">{{count($pengamal)}}</span>)</span> </td>
                   	</tr>
                   	<tr>
                   		<td colspan="1" style="text-align: right;">Total</td>
                   		<td>
                   			<input type="text" name="db_total" readonly="" placeholder="Rp.0" value="0" id="db-total">
                   		</td>
                   		<td>
                   			<input type="text" name="sp_total" readonly="" placeholder="Rp.0" value="0" id="sp-total">
                   		</td>
                   		<td>
                   			<input type="text" name="total" readonly="" placeholder="Rp.0" value="0" id="setor-total">
                   		</td>
                   	</tr>
                   	<tr>
                   		<td colspan="3" style="text-align: right;">10% Penghimpun</td>
                   		<td>
                   			<input type="text" name="total" readonly="" placeholder="Rp.0" value="0" id="setor-himpun">
                   		</td>
                   	</tr>
                </tbody>
              </table>
            </div>
            <div class="row">

            	<div class="col-12">
            		<div style="display: block; border-top: 1px solid #ddd; width: 100%; margin: 10px 0;"></div>
            		<label>Keterangan Tambahan</label>
            		<textarea name="description" class="form-control"></textarea>
            	</div>
            	<div class="col-12 mt-10">
            		<a href="javascript:void(0)" onclick="document.getElementById('setor-form').submit()" class="btn btn-primary pull-right">Simpan</a>
            	</div>
            </div>
		</div>
	</form>
</div>
</div>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Data Pengamal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="add-form-wrap">
        	<div class="form-group">
        		<label>Nama</label>
        		<select class="form-control" id="pengamal-list">

        		</select>
        	</div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="button" id="btn-add-people" class="btn btn-primary">Tambahkan</button>
      </div>
    </div>
  </div>
</div>

@endsection


@section('page-script')

<script type="text/x-tmpl" id="tmpl-list">
<tr class="setor-item" id="setor-item{%=o.id%}" data-id="{%=o.id%}" data-alamat="{%=o.alamat%}">
	<td>
		<a href="javascript:void(0)" data-id="{%=o.id%}" class="setor-delete"><i class="fa fa-times"></i></a>
		<input type="hidden" name="setordetail[{%=o.idpengamal%}][id_pengamal]" class="select-pengamal" value="{%=o.idpengamal%}" id="select-pengamal-id-{%=o.id%}">
		<input type="text" style="width:100%;" id="select-pengamal-nama-{%=o.id%}" value="{%=o.nama%} ({%=o.alamat%})" readonly>
	</td>
	<td>
		<input class="db-input" style="padding: 1px 10px !important;" type="text" name="setordetail[{%=o.idpengamal%}][db]" class="" placeholder="Rp. 0" id="setor-db-{%=o.id%}" value="0">
	</td>
	<td>
		<input class="sp-input" style="padding: 1px 10px !important;"  type="text" name="setordetail[{%=o.idpengamal%}][sp]" class="" placeholder="Rp. 0" id="setor-sp-{%=o.id%}" value="0">
	</td>
	<td>
		<input style="padding: 1px 10px !important;" class="setor-jumlah" id="setor-jumlah-{%=o.id%}" type="text" name="" disabled="" value="Rp. 0">
	</td>
	<td>
		<input type="text" class="setor-keterangan" name="setordetail[{%=o.idpengamal%}][detail]">
	</td>
</tr>
</script>

<script type="text/javascript">
	$(document).ready(function(){

		var currentID = 0;
		var listCount = 0;

		var pengamal = [
			@foreach($pengamal as $pk => $pv)
			{
				id: '{{$pv->id}}',
				nama:'{{$pv->nama}}',
				alamat: '{{$pv->alamat}}',
			},
			@endforeach
		];

		$("#setor-db-1").priceFormat({
			prefix: 'Rp. ',
			    centsSeparator: ',',
			    thousandsSeparator: '.',
			    centsLimit: 0,
		});
		$("#setor-sp-1").priceFormat({
			prefix: 'Rp. ',
			    centsSeparator: ',',
			    thousandsSeparator: '.',
			    centsLimit: 0,
		});
		$("#setor-total").priceFormat({
			prefix: 'Rp. ',
			    centsSeparator: ',',
			    thousandsSeparator: '.',
			    centsLimit: 0,
		});
		$("#db-total").priceFormat({
			prefix: 'Rp. ',
			    centsSeparator: ',',
			    thousandsSeparator: '.',
			    centsLimit: 0,
		});
		$("#sp-total").priceFormat({
			prefix: 'Rp. ',
			    centsSeparator: ',',
			    thousandsSeparator: '.',
			    centsLimit: 0,
		});
		$("#setor-himpun").priceFormat({
			prefix: 'Rp. ',
			    centsSeparator: ',',
			    thousandsSeparator: '.',
			    centsLimit: 0,
		});


		$.fn.insertNewItem = function(pengamal){

			var newId = currentID+1;
			var data = {
				"id": newId,
				"idpengamal": pengamal.id,
				"nama": pengamal.nama,
				"alamat": pengamal.alamat
			}
			var str = tmpl("tmpl-list", data);
			$("#last-sort").before(str);

			$(document).find('#setor-db-'+newId).priceFormat({
			    prefix: 'Rp. ',
			    centsSeparator: ',',
			    thousandsSeparator: '.',
			    centsLimit: 0,
			});
			$(document).find('#setor-sp-'+newId).priceFormat({
			    prefix: 'Rp. ',
			    centsSeparator: ',',
			    thousandsSeparator: '.',
			    centsLimit: 0,
			});
			$(document).find('#setor-jumlah-'+newId).priceFormat({
			    prefix: 'Rp. ',
			    centsSeparator: ',',
			    thousandsSeparator: '.',
			    centsLimit: 0,
			});

			listCount+=1;
			currentID+=1;

			$("#exampleModal").modal('toggle');
		};

		$.fn.pengamalChecker = function(){

			$("#pengamal-list").empty();
			$.each(pengamal, function(index, value){
				var canprint = 1;
				$(document).find(".select-pengamal").each(function(e){

					if($(this).val() == value.id){
						canprint = 0;
					}
				});
				if(canprint == 1){
					$("#pengamal-list").append(
						'<option value="'+value.id+'" data-id="'+value.id+'" data-nama="'+value.nama+'" data-alamat="'+value.alamat+'">'+value.nama+' ('+ value.alamat +')</option>'
					);
				}
			});
			$("#exampleModal").modal('toggle');
			return 0;
		}

		$.fn.totalChecker = function(){
			var totalall = 0;
			var totaldb = 0;
			var totalsp = 0;
			$(document).find('.setor-item').each(function(e){
				var id = $(this).data('id');

				var db = parseInt($(document).find('#setor-db-'+id).unmask());
				var sp = parseInt($(document).find('#setor-sp-'+id).unmask());
				totalsp += sp;
				totaldb += db;
				var total = db+sp;
				totalall += total;
				$(document).find('#setor-jumlah-'+id).val(total);
				$(document).find('#setor-jumlah-'+id).priceFormat({
				    prefix: 'Rp. ',
				    centsSeparator: ',',
				    thousandsSeparator: '.',
				    centsLimit: 0,
				});
			});

			$("#setor-total").val(totalall);
			$("#setor-total").priceFormat({
			    prefix: 'Rp. ',
			    centsSeparator: ',',
			    thousandsSeparator: '.',
			    centsLimit: 0,
			});
			$("#db-total").val(totaldb);
			$("#db-total").priceFormat({
			    prefix: 'Rp. ',
			    centsSeparator: ',',
			    thousandsSeparator: '.',
			    centsLimit: 0,
			});
			$("#sp-total").val(totalsp);
			$("#sp-total").priceFormat({
			    prefix: 'Rp. ',
			    centsSeparator: ',',
			    thousandsSeparator: '.',
			    centsLimit: 0,
			});
			if(totaldb > 100){
				$("#setor-himpun").val((10/100)*totaldb);
				$("#setor-himpun").priceFormat({
				    prefix: 'Rp. ',
				    centsSeparator: ',',
				    thousandsSeparator: '.',
				    centsLimit: 0,
				});
			} else {
				$("#setor-himpun").val(0);
				$("#setor-himpun").priceFormat({
				    prefix: 'Rp. ',
				    centsSeparator: ',',
				    thousandsSeparator: '.',
				    centsLimit: 0,
				});
			}
		};

		$(document).on('keyup', '.sp-input', function(e){
			$.fn.totalChecker();
		});

		$(document).on('keyup', '.db-input', function(e){
			$.fn.totalChecker();
		});

		$(document).on('click', '#add-row', function(){
			console.log(listCount + " : " +pengamal.length);
			if(listCount == pengamal.length){
				swal("Maaf!", "Semua data pengamal sudah dimasukan", "warning");

				return 0;
			} else {
				$.fn.pengamalChecker();
				return 0;
			}

		});

		$(document).on('click', '#btn-add-people', function(){
			var pid = $("#pengamal-list").find(":selected").data('id');
			var pnama = $("#pengamal-list").find(":selected").data('nama');
			var palamat = $("#pengamal-list").find(":selected").data('alamat');
			var item = {
				id:pid, nama:pnama, alamat:palamat
			};
			$.fn.insertNewItem(item);
			$("#pengamal-count").text(pengamal.length - listCount);
		});

		$(document).on('click', '.setor-delete', function(e){
			if(listCount > 1){
				var id = $(this).data('id');
				$(document).find('#setor-item'+id).remove();
				listCount-=1;
				$.fn.totalChecker();
				$("#pengamal-count").text(pengamal.length - listCount);
			} else {
				swal("Maaf!", "Data tidak boleh kosong!", "warning");
			}

		});
	});

</script>

@endsection
