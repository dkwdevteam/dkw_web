<nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li> <a class="waves-effect waves-dark" href="{{url('/')}}" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>
                        </li>

                         <li> <a class="waves-effect waves-dark" href="{{url('notifikasi')}}" aria-expanded="false"><i class="mdi mdi-bell-outline"></i><span class="hide-menu">Notifikasi</span></a>
                        </li>


                        @if(Sentinel::getUser())
                        @if((Sentinel::getUser()->inRole('super-admin'))||(Sentinel::getUser()->inRole('admin-pusat'))||(Sentinel::getUser()->inRole('admin-provinsi'))||(Sentinel::getUser()->inRole('admin-kabko')))
                          <li><a class="waves-effect waves-dark" href="{{url('usersadministration')}}" aria-expanded="false"><i class="mdi mdi-account-card-details"></i><span class="hide-menu">Pengguna</span></a>
                          </li>
                        @endif
                        @endif

                        @if(Sentinel::getUser())
                        @if(Sentinel::getUser()->inRole('super-admin'))
                        <li><a class="waves-effect waves-dark" href="{{url('roles')}}" aria-expanded="false"><i class="mdi mdi-certificate"></i><span class="hide-menu">Roles</span></a>
                        </li>
                        @endif
                        @endif



                        {{-- <li> <a class="waves-effect waves-dark" href="{{url('manageuser')}}" aria-expanded="false"><i class="mdi mdi-account-card-details"></i><span class="hide-menu">Kelola User</span></a>
                        </li> --}}

                        @if(Sentinel::getUser()->inRole('admin-kecamatan'))
                        <li> <a class="waves-effect waves-dark" href="{{url('jamaah')}}" aria-expanded="false"><i class="mdi mdi-account-check"></i><span class="hide-menu">Kelola Jamaah</span></a>
                        </li>

                        @endif

                        @if(Sentinel::getUser()->inRole('admin-kecamatan'))
                        <li> <a class="waves-effect waves-dark" href="{{url('setoran/kecamatan')}}" aria-expanded="false"><i class="mdi mdi-table"></i><span class="hide-menu">Setoran Kecamatan</span></a>
                        </li>
                        @endif
                        @if(Sentinel::getUser()->inRole('admin-kecamatan'))
                        <li> <a class="waves-effect waves-dark" href="{{url('setoran/kecamatan/history')}}" aria-expanded="false"><i class="mdi mdi-table"></i><span class="hide-menu">Sejarah Setoran</span></a>
                        </li>
                        @endif

                        @if(Sentinel::getUser()->inRole('admin-kabko'))
                        <li> <a class="waves-effect waves-dark" href="{{url('setoran/kabupaten')}}" aria-expanded="false"><i class="mdi mdi-table"></i><span class="hide-menu">Setoran Kab/Ko</span></a>
                        </li>
                        @endif
                        @if(Sentinel::getUser()->inRole('admin-kabko'))
                        <li> <a class="waves-effect waves-dark" href="{{url('setoran/kabupaten/history')}}" aria-expanded="false"><i class="mdi mdi-table"></i><span class="hide-menu">Sejarah Setoran</span></a>
                        </li>
                        @endif

                        @if(Sentinel::getUser()->inRole('admin-provinsi'))
                        <li> <a class="waves-effect waves-dark" href="{{url('setoran/provinsi/history')}}" aria-expanded="false"><i class="mdi mdi-table"></i><span class="hide-menu">Sejarah Setoran</span></a>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="{{url('setoran/provinsi/rekapitulasi')}}" aria-expanded="false"><i class="mdi mdi-file"></i><span class="hide-menu">Rekapitulasi Setoran</span></a>
                        </li>
                        @endif

                        @if(Sentinel::getUser()->inRole('admin-pusat'))
                        <li> <a class="waves-effect waves-dark" href="{{url('setoran/pusat/history')}}" aria-expanded="false"><i class="mdi mdi-table"></i><span class="hide-menu">Sejarah Setoran</span></a>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="{{url('buktisetoran/pusat')}}" aria-expanded="false"><i class="mdi mdi-google-photos"></i><span class="hide-menu">Bukti Setoran</span></a>
                        </li>
                        @endif


                        {{-- <li> <a class="waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-help-circle"></i><span class="hide-menu">Error 404</span></a>
                        </li> --}}
                    </ul>
                    {{-- <div class="text-center m-t-30">
                        <a href="https://themewagon.com/themes/bootstrap-4-responsive-admin-template/" class="btn waves-effect waves-light btn-warning hidden-md-down">Download Now</a>
                    </div> --}}
                </nav>
