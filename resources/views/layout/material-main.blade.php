<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/images/favicon.png') }}">
    <title>{{ config('app.name') }}</title>
    <!-- Bootstrap Core CSS -->
    <!-- <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
    {!!Html::style('assets/plugins/bootstrap/css/bootstrap.min.css')!!}
    <!-- Popup CSS -->
    {!!Html::style('css/magnific-popup.css')!!}
    <!-- chartist CSS -->
    <!-- <link href="assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet"> -->
    {!!Html::style('assets/plugins/chartist-js/dist/chartist.min.css')!!}
    <!-- <link href="assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet"> -->
    {!!Html::style('assets/plugins/chartist-js/dist/chartist-init.css')!!}
    <!-- <link href="assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet"> -->
    {!!Html::style('assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')!!}
    <!--This page css - Morris CSS -->
    <!-- <link href="assets/plugins/c3-master/c3.min.css" rel="stylesheet"> -->
    {!!Html::style('assets/plugins/c3-master/c3.min.css')!!}
    {!!Html::style('assets/plugins/select2/select2.min.css')!!}
    <!-- Custom CSS -->
    <!-- <link href="css/style.css" rel="stylesheet"> -->
    {!!Html::style('css/style.css')!!}
    <!-- You can change the theme colors from here -->
    <!-- <link href="css/colors/blue.css" id="theme" rel="stylesheet"> -->
    {!!Html::style('css/colors/blue.css')!!}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    {!!Html::style('css/jquery.dataTables.min.css')!!}
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-toggleable-sm navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="{{url('/')}}">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->

                            <!-- Light Logo icon -->
                            <img src="{{url('assets/images/logo-light-icon.png')}}" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span> {{ config('app.name') }} </span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i class="ti-close"></i></a> </form>
                        </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{url('assets/images/users/1.jpg')}}" alt="user" class="profile-pic"></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="{{url('assets/images/users/1.jpg')}}" alt="user"></div>
                                            <div class="u-text">
                                                <h4>@if((!$userprofile->first_name)||(!$userprofile->last_name))
                                                John Doe
                                                @else
                                                {{$userprofile->first_name}}&nbsp;{{$userprofile->last_name}}
                                                @endif</h4>
                                                <p class="text-muted">
                                                  @if(!$userprofile->email)
                                                  JohnDoe@gmail.com
                                                  @else
                                                  {{$userprofile->email}}
                                                  @endif
                                                </p><a href="{{url('/userprofile')}}" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{url('/userprofile')}}"><i class="ti-user"></i> My Profile</a></li>
                                    <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
                                    <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="{{url('logout')}}"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                @include('layout.sidebar')
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
            <!-- Bottom points-->
            <div class="sidebar-footer">
              <!-- item--><a href="#" class="link" data-toggle="tooltip" data-animation="false"title="Setting"><i class="ti-settings"></i></a>
                <!-- item--><a href="#" class="link" data-toggle="tooltip" data-animation="false"title="Email"><i class="mdi mdi-email"></i></a>
                <!-- item--><a href="{{url('logout')}}" class="link" data-toggle="tooltip" data-animation="false"title="Logout"><i class="mdi mdi-power"></i></a> </div>
            <!-- End Bottom points-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                @if(!Request::is('/'))
                <div class="clearfix" style="padding-bottom: 20px;"></div>
                @endif
                @yield('content')
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php
            $currUser = App\Models\Users::find(Sentinel::check()->id);
            $userRoles =Sentinel::findById($userprofile->id)->roles()->get();?>
            <footer class="footer"> © {{date('Y')}} {{ config('app.name') }} - Login as
              <b class="text-success">@foreach($userRoles as $sk)
                {{$sk->name}}
                @switch($sk->slug)
                    @case("admin-provinsi")
                        <span> - {{$currUser->getProvince->name}}</span>
                        @break
                    @case("admin-kabko")
                        <span> - {{$currUser->getCities->name}}</span>
                        @break
                    @case("admin-kecamatan")
                        <span> - {{$currUser->getKecamatan->name}}</span>
                        @break
                    @default
                        <span></span>
                @endswitch
              @endforeach</b>
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <!-- <script src="assets/plugins/jquery/jquery.min.js"></script> -->
    {!!Html::script('assets/plugins/jquery/jquery.min.js')!!}
    {!!Html::script('assets/plugins/jquery/html2canvas.js')!!}
    <!-- Bootstrap tether Core JavaScript -->
    <!-- <script src="assets/plugins/bootstrap/js/tether.min.js"></script> -->
    {!!Html::script('assets/plugins/bootstrap/js/tether.min.js')!!}
    <!-- <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> -->
    {!!Html::script('assets/plugins/bootstrap/js/bootstrap.min.js')!!}
    <!-- slimscrollbar scrollbar JavaScript -->
    <!-- <script src="js/jquery.slimscroll.js"></script> -->
    {!!Html::script('js/jquery.slimscroll.js')!!}
    <!--Wave Effects -->
    <!-- <script src="js/waves.js"></script> -->
    {!!Html::script('js/waves.js')!!}
    <!--Menu sidebar -->
    <!-- <script src="js/sidebarmenu.js"></script> -->
    {!!Html::script('js/sidebarmenu.js')!!}
    <!--stickey kit -->
    <!-- <script src="assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script> -->
    {!!Html::script('assets/plugins/sticky-kit-master/dist/sticky-kit.min.js')!!}
    <!--Custom JavaScript -->
    <!-- <script src="js/custom.min.js"></script> -->
    {!!Html::script('js/custom.min.js')!!}
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- chartist chart -->
    <!-- <script src="assets/plugins/chartist-js/dist/chartist.min.js"></script> -->

    <!-- Magnific popup JavaScript -->
    {!!Html::script('js/jquery.magnific-popup.min.js')!!}
    {!!Html::script('js/jquery.magnific-popup-init.js')!!}
    <!--c3 JavaScript -->
    <!-- <script src="assets/plugins/d3/d3.min.js"></script> -->
    {!!Html::script('assets/plugins/d3/d3.min.js')!!}
    <!-- <script src="assets/plugins/c3-master/c3.min.js"></script> -->
    {!!Html::script('assets/plugins/c3-master/c3.min.js')!!}
    {!!Html::script('assets/plugins/select2/select2.full.min.js')!!}
    <!-- Chart JS -->
    <!-- <script src="js/dashboard1.js"></script> -->
    {!!Html::script('js/dashboard1.js')!!}

    {!!Html::script('js/jquery.priceformat.min.js')!!}
    <!-- <script src="assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script> -->
    {!!Html::script('js/jquery.dataTables.min.js')!!}
    {!!Html::script('js/dataTables.fixedHeader.min.js')!!}

    {!!Html::script('assets/plugins/chartist-js/dist/chartist.min.js')!!}
    {!!Html::script('assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js')!!}
    {!!Html::script('https://cdnjs.cloudflare.com/ajax/libs/blueimp-JavaScript-Templates/3.11.0/js/tmpl.min.js') !!}
    {!!Html::script('https://cdnjs.cloudflare.com/ajax/libs/blueimp-JavaScript-Templates/3.11.0/js/tmpl.min.js.map')!!}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<div id="PrintThisHidden" style="display: none;"></div>

<script>
html2canvas($("#printThis"), {
    onrendered: function(canvas) {
        var canvasImg = canvas.toDataURL("image/jpg");
        $('#PrintThisHidden').html('<img src="'+canvasImg+'" alt="">');
    }
});

$("#buttonPrint").click(function(e){
    var printContent = document.getElementById("PrintThisHidden");
    var printWindow = window.open("", "","left=50,top=50");
    printWindow.document.write(printContent.innerHTML);
    printWindow.document.write("<script src=\'http://code.jquery.com/jquery-1.10.1.min.js\'><\/script>");
    printWindow.document.write("<script>$(window).load(function(){ print(); close(); });<\/script>");
    printWindow.document.close();
});
</script>
</body>

@yield('page-script')
</html>
