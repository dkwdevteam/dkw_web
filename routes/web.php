<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('inituser', function(){
	// $role = Sentinel::getRoleRepository()->createModel()->create([
	//     'name' => 'Admin Kota',
	//     'slug' => 'admin-kota',
	// ]);
	$role = Sentinel::findRoleById('2');
	$credentials = [
	    'email'    => 'j@dkwapp.com',
	    'password' => '@Password123',
			'username' => 'j'
	];
	$user = Sentinel::registerAndActivate($credentials);
	$role->users()->attach($user);
});

Route::get('cleantest', function(){
	$value = "Rp. 120.000 . . . .";
	$value = str_replace('Rp.', '', $value);
	$value = str_replace('.', '', $value);
	$value = str_replace(' ', '', $value);
	print($value);
});

Route::group(['middleware' => 'sentinelmember'], function(){
	Route::get('/', 'UserController@login');

	Route::group(['prefix' => 'usersadministration'], function(){
		Route::get('/', 'UsersAdministrationController@index');
		// Route::get('/search', 'UsersAdministrationController@search');
		Route::post('/','UsersAdministrationController@store');
		Route::get('/create', 'UsersAdministrationController@create');
		// Route::patch('/{id}', 'UsersAdministrationController@update');
		Route::patch('/epass/{id}', 'UsersAdministrationController@updatepass');
		// Route::get('/{id}', 'UsersAdministrationController@showedit');
		Route::get('/show/{id}', 'UsersAdministrationController@show');
		Route::get('/pass/{id}', 'UsersAdministrationController@showpass');
		// Route::delete('/{id}', 'UsersAdministrationController@destroy');
	});

	Route::group(['prefix' => 'jamaah'], function(){
		Route::get('/', 'JamaahController@index');
	});

	Route::group(['prefix' => 'manageuser'], function(){
		Route::get('/', 'UserManagerController@index');
		Route::get('create', 'UserManagerController@add');
	});

	Route::group(['prefix' => 'setoran'], function(){
		Route::get('/', 'SetoranController@setoran');

		Route::group(['prefix' => 'kecamatan'], function(){
			Route::get('/', 'SetoranKecamatanController@setoranDbSpKecamatan');
			Route::post('store', 'SetoranKecamatanController@storeDeposit');
			Route::get('history', 'SetoranKecamatanController@history');
			Route::get('details/{id}', 'SetoranKecamatanController@details');
		});
		Route::group(['prefix' => 'kabupaten'], function(){
			Route::get('/', 'SetoranKabupatenController@setoranDbSpKabupaten');
			Route::post('store', 'SetoranKabupatenController@storeDeposit');
			Route::post('prestore', 'SetoranKabupatenController@preStoreDeposit');
			Route::get('history', 'SetoranKabupatenController@history');
			Route::get('details/{id_month}/{id_year}/{id_status}', 'SetoranKabupatenController@details');
			Route::get('subdetails/{id}', 'SetoranKabupatenController@subdetails');
			Route::post('accept/{id}', 'SetoranKabupatenController@acceptSetoran');
		});
		Route::group(['prefix' => 'provinsi'], function(){
			Route::get('history', 'SetoranProvinsiController@history');
			Route::get('details/{id_month}/{id_year}/{id_status}', 'SetoranProvinsiController@details');
			Route::get('subdetails/{id_month}/{id_year}/{id_status}/{id_city}', 'SetoranProvinsiController@subdetails');
			Route::get('subdetails2/{id_month}/{id_year}/{id_status}/{id_city}/{id}', 'SetoranProvinsiController@subdetails_kecamatan');
			Route::get('rekapitulasi', 'SetoranProvinsiController@rekapitulasi');
		});
		Route::group(['prefix' => 'pusat'], function(){
			Route::get('history', 'SetoranPusatController@history');
			Route::get('details/{id_month}/{id_year}/{id_status}', 'SetoranPusatController@details');
			Route::get('subdetails/{id_month}/{id_year}/{id_status}/{id_region}', 'SetoranPusatController@subdetails');
			Route::get('subdetails2/{id_month}/{id_year}/{id_status}/{id_region}/{id_city}', 'SetoranPusatController@subdetails_kabko');
			Route::get('subdetails3/{id_month}/{id_year}/{id_status}/{id_region}/{id_city}/{id}', 'SetoranPusatController@subdetails_kecamatan');
		});
	});

	Route::group(['prefix' => 'buktisetoran'], function(){
		Route::group(['prefix' => 'pusat'], function(){
			Route::get('/', 'BuktiSetoranPusatController@index');
			Route::get('details/{id}', 'BuktiSetoranPusatController@details');
		});
	});

	Route::group(['prefix' => 'notifikasi'], function(){
		Route::get('/', 'NotifikasiController@index');
	});
});

Route::group(['prefix' => 'api'], function(){
	Route::group(['prefix' => 'v1'], function(){
		Route::post('getcity', 'API\V1@getCity');
		Route::post('getdistrict', 'API\V1@getDistrict');

		//people
		Route::post('insertpeople', 'JamaahController@insertPeople');
		Route::post('editpeople', 'JamaahController@editDataPeople');
		Route::post('loadpeople', 'JamaahController@loadDataPeople');
		Route::post('deletepeople', 'JamaahController@deletePeople');
		Route::post('getlistkk', 'JamaahController@loadListKK');
		Route::post('getdatapeople', 'JamaahController@getDataPeople');

	});
});


Route::group(['middleware' => 'SAmember'], function(){
	Route::group(['prefix' => 'roles'], function(){
		Route::get('/', 'RolesController@index');
		Route::get('/search', 'RolesController@search');
		Route::post('/','RolesController@store');
		Route::get('/create', 'RolesController@create');
		Route::patch('/{id}', 'RolesController@update');
		Route::get('/{id}', 'RolesController@show');
		Route::delete('/{id}', 'RolesController@destroy');
	});
});

// USER LOGIN
Route::get('userprofile/{id}','UserController@edit');
Route::get('userpassword/{id}','UserController@editPassword');
Route::get('login', 'UserController@login');
Route::post('login', 'UserController@postLogin');
Route::get('userprofile','UserController@userProfile');
Route::patch('userprofile/{id}', 'UserController@update');
Route::patch('userpassword/{id}', 'UserController@updatePassword');
Route::get('logout','UserController@logout');

// No Permission
Route::get('nopermission',function(){
	$user = Sentinel::getUser();
	return view('layout.contents.nopermission.index', array(
		'userprofile'=>$user,
		'url'=>url()->previous()
	));
});

// Setoran DB dan SP Kecamatan
